#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
# sets up customfields, if they don't exist, and generates bugs_customfields entries.
# Should be run once to upgrade database from old to new config.

###########################################################################
# Global definitions
###########################################################################

use diagnostics;
use strict;

use AnyDBM_File;

use lib qw(.);

require "globals.pl";
use vars qw($template $vars);

print "***STOP***\n\n";
print "Are you sure what you are doing?\n\n";
print "This script is pure evil. Bits might go wrong. Unless you are confident\n";
print "that you know how to fix up the database if it goes wrong, don't run it!\n\n";
print "Edit the script itself to actually make it run (remove the exit command).\nYou have been warned.\n";


ConnectToDatabase(1);
GetVersionTable();

my @row;
my @sql_data = ();

# Setup initial customfields/products_customfields entries
SendSQL("DELETE FROM customfields");
SendSQL("INSERT INTO customfields VALUES (3,'gg_version','single','Unspecified|Unversioned Enhancement|2.0|2.1/2.2|2.3/2.4|2.5/2.6|2.7/2.8','Unspecified',0)");
SendSQL("INSERT INTO customfields VALUES (4,'gg_milestone','single','Unspecified|Old|2.6.0','Unspecified',0)");
# os_details and version_details are now added into the initial comment
SendSQL("DELETE FROM fielddefs WHERE name='os_details'");
SendSQL("DELETE FROM fielddefs WHERE name='version_details'");
SendSQL("DELETE FROM fielddefs WHERE name='gg_version'");
SendSQL("DELETE FROM fielddefs WHERE name='gg_milestone'");
SendSQL("INSERT INTO fielddefs (name, description, mailhead) VALUES ('gg_version', 'GNOME Version', 0)");
SendSQL("INSERT INTO fielddefs (name, description, mailhead) VALUES ('gg_milestone', 'GNOME Milestone', 0)");

SendSQL("DELETE FROM attachstatusdefs");
SendSQL("INSERT INTO attachstatusdefs (id, description, name, product, sortkey) VALUES ('0', 'The maintainer has given commit permission', 'maintainer-ok', '_gg_global', '999')");
SendSQL("INSERT INTO attachstatusdefs (id, description, name, product, sortkey) VALUES ('1', 'The patch needs work', 'needs-work', '_gg_global', '1000')");

if (0) {
    
SendSQL("DELETE FROM bugs_customfields where cf_id=1");
SendSQL("DELETE FROM bugs_customfields where cf_id=2");

SendSQL("SELECT bug_id, op_sys_details, version_details FROM bugs");

my @row;
my @sql_data = ();
while ( @row = FetchSQLData() ) {
    push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 1, " .  SqlQuote($row[1]) . ")");
    push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 2, " .  SqlQuote($row[2]) . ")");
}

foreach my $value(@sql_data) {
    SendSQL($value);
}
 
}

if (1) {
@sql_data = ();
my $version_done = 0;
my $milestone_done = 0;
my $id = 0;

SendSQL("DELETE FROM bugs_customfields where cf_id=3");
SendSQL("DELETE FROM bugs_customfields where cf_id=4");

SendSQL("SELECT bugs.bug_id, keywordid, bugs.bug_severity FROM bugs left join keywords using (bug_id) ORDER BY bug_id");

while ( @row = FetchSQLData() ) {
    if ($id != $row[0]) {
		if (!$version_done) {
    	    push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($id, 3, 'Unspecified')");
		}
		if (!$milestone_done) {
    	    push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($id, 4, 'Unspecified')");
		}
		$version_done = 0;
		$milestone_done = 0;
		$id = $row[0];
    }

	if ($row[2] eq "enhancement") {
		push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 3, 'Unversioned Enhancement')");
		$version_done = 1;
	}

	if ($row[1] && $version_done != 1) {
		if ($row[1] == '33') {
			push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 3, '1.4')");
			$version_done = 1;
		}

		if ($row[1] == '20') {
			push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 3, '2.0')");
			$version_done = 1;
		}

		if ($row[1] == '44' || $row[1] == '10') {
			push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 3, '2.1/2.2')");
			$version_done = 1;
		}

		if ($row[1] == '9' || $row[1] == '50') {
			push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 3, '2.3/2.4')");
			$version_done = 1;
		}

		if ($row[1] == '53') {
			push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 3, '2.5/2.6')");
			$version_done = 1;
		}

		if ($row[1] == '55') {
			push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 3, '2.7/2.8')");
		}
	}

	if ($row[1]) {
		
		if ($row[1] == '32' || $row[1] == '31' || $row[1] == '17' || $row[1] == '41' || $row[1] == '42' || $row[1] == '21' || $row[1] == '47' || $row[1] == '48' || $row[1] == '37' || $row[1] == '51' || $row[1] == '52') {
			push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 4, 'Old')");

			$milestone_done = 1;
		}
	}

	if ($row[1]) {
		if ($row[1] == '54') {
			push (@sql_data, "INSERT INTO bugs_customfields (bug_id, cf_id, value) VALUES ($row[0], 4, '2.6.0')");
			$milestone_done = 1;
		}
	}
}

foreach my $value(@sql_data) {
    SendSQL($value);
}
}

unlink "data/versioncache";
