#!/usr/bonsaitools/bin/perl -w
# 
# simple-bug-guide.cgi
# This page is linked to from within GNOME and should *always* be accessible
# from http://bugzilla.gnome.org/simple-bug-guide.cgi
# 
# It takes these arguments. It's OK to call it with no arguments or any
# combination of the below; it tries to Do The Right Thing
# 
# sbg_type: what type of bug we're filing. This is used to determine what sort of
# template to use for the bug entry, and also affects what questions the user
# gets asked to determine values for other unset arguments. Valid values are:
#   normal
#   crasher
#   usability
#   docs
#   enhancement
#   l10n (translation issues)
# It's case sensitive. Deal with it.
# 
# product
# application - exactly the same as product unless you're filing an l10n bug.
# comp
# severity
# priority
# version
# gg_version (GNOME Version as per the customfield)
# 
# Copyright (c) Andrew Sobala 2004.
# Licensed under the GNU General Public License Version 2 and the Mozilla
# Public License.

use diagnostics;

use lib qw(.);
require "CGI.pl";

ConnectToDatabase ();
GetVersionTable ();

use vars qw (
    $vars
    %COOKIE
    %versions
    @customfields
    @legal_customfields
    $sbg_type
    $product
    $application
    $comp
    $severity
    $priority
    $version
    $gg_version
);

$sbg_type = $::FORM{'sbg_type'};
$product = $::FORM{'product'};
$application = $::FORM{'application'};
$comp = $::FORM{'comp'};
$severity = $::FORM{'severity'};
$priority = $::FORM{'priority'};
$version = $::FORM{'version'};
$gg_version = $::FORM{'gg_version'};

# Some standard vars
$vars->{'format'} = $::FORM{'format'};

##
# A bit straight from enter_bug.cgi

my %products;
my %gnome_products;

foreach my $p (@enterable_products) {
    if (!(Param("usebuggroupsentry") 
          && GroupExists($p) 
          && !UserInGroup($p)))
    {
        $products{$p} = $::proddesc{$p};
        if ($::isgnome{$p} == 1)
        {
            $gnome_products{$p} = $::proddesc{$p};
        }
    }
}
my $prodsize = scalar(keys %products);
if ($prodsize == 0) {
    DisplayError("Either no products have been defined to enter bugs ".
                 "against or you have not been given access to any.\n");
    exit;
} 
elsif ($prodsize > 1) {
    $vars->{'proddesc'} = \%products;
    $vars->{'gg_proddesc'} = \%gnome_products;
}

# Let's go.
# Main application logic here :)

confirm_login ();

#### TYPE
if (!defined ($sbg_type)) {
    if ($severity eq "enhancement") {
        $sbg_type = "enhancement";
    }
    if ($product eq "l10n") {
        $sbg_type = "l10n";
    }
}

if (!defined ($sbg_type)) {
    my $format = ValidateOutputFormat($::FORM{'format'}, "select-type", "bug/sbg");

    copy_to_vars ();
    print "Content-type: $format->{'contenttype'}\n\n";
    $template->process("bug/sbg/$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit ();
}

#### APPLICATION AND PRODUCT
# "calling this script after defining product and application to be set but
# different is unsupported, unless you're sure what you're doing is right"
# or something
# (this is real voodoo)

if (defined ($product) && !defined($application) && $product ne "l10n") {
	$application = $product;
}
if (defined ($application) && !defined ($product)) {
    $product = $application;
}
if ($sbg_type eq "l10n") {
    $product = "l10n";
}

if (!defined ($application)) {
    my $format = ValidateOutputFormat($::FORM{'format'}, "select-product", "bug/sbg");

    copy_to_vars ();
    print "Content-type: $format->{'contenttype'}\n\n";
    $template->process("bug/sbg/$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit ();
}

#### COMPONENT
if (!defined ($comp)) {
# from enter_bug.cgi
    if (lsearch(\@::enterable_products, $product) == -1) {
        DisplayError("'" . html_quote($product) . "' is not a valid product.");
        exit;
    }
    
    if (0 == @{$::components{$product}}) {
        my $error = "Sorry; there needs to be at least one component for this " .
                    "product in order to create a new bug. ";
        if (UserInGroup('editcomponents')) {
            $error .= "<a href=\"editcomponents.cgi\">" . 
                      "Create a new component</a>\n";
        }
        else {              
            $error .= "Please contact " . Param("maintainer") . ", detailing " .
                      "the product in which you tried to create a new bug.\n";
        }
    
        DisplayError($error);   
        exit;
    } 
    elsif (1 == @{$::components{$product}}) {
        # Only one component; just pick it.
        $comp = $::components{$product}->[0];
    }
    elsif (($sbg_type eq "docs") &&
           (my @doccomps = grep(lc($_) eq "docs", @{$::components{$product}}))) {
        # For documentation bugs automatically select docs component
        # Note: Not copy pasted from enter_bug.cgi
        $comp = $doccomps[0];
    }

    my @components;
    
    # Retrieve the descriptions for every component but the 'docs'.
    # The docs component would already be selected above
    # Note: Not selecting 'docs' is not from enter_bug.cgi
    SendSQL("SELECT value, description FROM components " . 
            "WHERE value != 'docs' AND program = " . SqlQuote($product) . " " .
            "ORDER BY value");
    while (MoreSQLData()) {
        my ($name, $description) = FetchSQLData();
    
        my %component;

        $component{'name'} = $name;
        $component{'description'} = $description;
    
        push @components, \%component;
    }
# end enter_bug.cgi
    
    # It is possible that there are only two components, and one of them
    # is 'docs'. In that case we currently only have one component.
    # If so, select that one
    if (!defined($comp) && (0 == $#components)) {
        $comp = $components[0]{'name'};
    }
    
    $vars->{'components'} = \@components;

    if (!defined ($comp)) {
        my $format = ValidateOutputFormat($::FORM{'format'}, "select-comp", "bug/sbg");

        copy_to_vars ();
        print "Content-type: $format->{'contenttype'}\n\n";
        $template->process("bug/sbg/$format->{'template'}", $vars)
            || ThrowTemplateError($template->error());
        exit ();
    }
}

#### SEVERITY AND PRIORITY
if (!defined ($severity)) {
    if ($sbg_type eq "enhancement") {
        $severity = "enhancement";
    }
    if ($sbg_type eq "crasher") {
        $severity = "critical";
        if (!defined ($priority)) {
            $priority = "High";
        }
    }
    if ($sbg_type eq "usability" || $sbg_type eq "docs") {
        $severity = "minor";
    }
    if ($sbg_type eq "l10n") {
        $severity = "normal";
    }
}
if (!defined ($priority)) {
    $priority = "Normal";
}
if (!defined ($severity)) {
    my $format = ValidateOutputFormat($::FORM{'format'}, "select-severity", "bug/sbg");

    copy_to_vars ();
    print "Content-type: $format->{'contenttype'}\n\n";
    $template->process("bug/sbg/$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit ();
}


#### VERSION AND GG_VERSION
if ($sbg_type eq "enhancement") {
    $gg_version = "Unversioned Enhancement";
}
if ($::isgnome{$product} != 1) {
    $gg_version = "Unspecified";
}
if (1 == @{$::versions{$product}}) {
    # Only one version; just pick it.
    $version = $::versions{$product}->[0];
}
if (!defined ($version) || !defined ($gg_version)) {
    $vars->{'versions'} = $::versions{$product};
    # custom fields
    for (@::customfields)  {
      
        my %cbug;
   
        $cbug{'name'} = $_; 
        $cbug{'values'} = [split('\|',$::legal_customfields{$_}[1])];
        $cbug{'type'} = $::legal_customfields{$_}[0];
        $cbug{'mandatory'} = $::legal_customfields{$_}[3];
        $cbug{'default'} = $::legal_customfields{$_}[4];
        $default{$cbug{'name'}} = $::legal_customfields{$_}[4];
 
        push @cfields, \%cbug;
    }
    $vars->{'customfield'} = \@cfields;

    my $format = ValidateOutputFormat($::FORM{'format'}, "select-version", "bug/sbg");

    copy_to_vars ();
    print "Content-type: $format->{'contenttype'}\n\n";
    $template->process("bug/sbg/$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit ();
}

#### BUG_TEXT
# Set keywords automatically
    if ($sbg_type eq "usability") {
	    $vars->{'keywords'} = "usability";
    }
    if ($sbg_type eq "docs") {
        $vars->{'keywords'} = "documentation";
    }
    if ($sbg_type eq "l10n") {
        $vars->{'keywords'} = "L10N";
    }
# we override format to be sbg_type for this last bit
    my $format = ValidateOutputFormat($sbg_type, "enter-bug", "bug/sbg");

    copy_to_vars ();
    print "Content-type: $format->{'contenttype'}\n\n";
    $template->process("bug/sbg/$format->{'template'}", $vars)
        || ThrowTemplateError($template->error());
    exit ();

sub copy_to_vars ()
{
    $vars->{'sbg_type'} = $sbg_type;
    $vars->{'product'} = $product;
    $vars->{'application'} = $application;
    $vars->{'comp'} = $comp;
    $vars->{'severity'} = $severity;
    $vars->{'priority'} = $priority;
    $vars->{'version'} = $version;
    $vars->{'gg_version'} = $gg_version;
}
