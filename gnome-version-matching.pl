# -*- Mode: perl; indent-tabs-mode: nil -*-

use diagnostics;
use strict;

sub gnome_find_version {
	my ($gnomestring) = @_;
	
	if ($gnomestring =~ /GNOME2\.0/) {
		return "2.0";
	} elsif ($gnomestring =~ /GNOME2\.[12]\./) {
		return "2.1/2.2";
	} elsif ($gnomestring =~ /GNOME2\.[34]/) {
		return "2.3/2.4";
	} elsif ($gnomestring =~ /GNOME2\.[56]/) {
		return "2.5/2.6";
	} elsif ($gnomestring =~ /GNOME2\.[78]/) {
		return "2.7/2.8";
	} elsif ($gnomestring =~ /GNOME2\.(?:9|10)\./) {
		return "2.9/2.10";
	} elsif ($gnomestring =~ /GNOME2\.1[12]./) {
		return "2.11/2.12";
	} elsif ($gnomestring =~ /GNOME2\.1[34]./) {
		return "2.13/2.14";
	} elsif ($gnomestring =~ /GNOME2\.1[56]./) {
		return "2.15/2.16";
	} else {
		return "Unspecified";
	}
}

1;
