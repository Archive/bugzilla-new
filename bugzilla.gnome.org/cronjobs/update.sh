#!/bin/bash
#
# This script is cron'ed to run every 30 minutes.

OUTPUT_DIR="`pwd`"
BUG_BUDDY_DIR=..
BUGZILLA_DIR=$OUTPUT_DIR/../..
REPORTS_DIR=$OUTPUT_DIR/../../reports

# Needed for running the cgi scripts
REQUEST_METHOD="GET"
export REQUEST_METHOD

(./create-products-xml.pl > temp.xml && iconv -c -f ASCII -t utf8 temp.xml > temp2.xml && rm temp.xml && mv temp2.xml $BUG_BUDDY_DIR/bugzilla-products.xml)
(xmllint --noout $BUG_BUDDY_DIR/bugzilla-products.xml 2>/dev/null; if [ $? = 1 ]; then xmllint --recover  $BUG_BUDDY_DIR/bugzilla-products.xml > temp.xml 2>/dev/null && mv -f temp.xml $BUG_BUDDY_DIR/bugzilla-products.xml; fi)
(./create-config-xml.pl > temp.xml && iconv -c -f ASCII -t utf8 temp.xml > temp2.xml && rm temp.xml && mv temp2.xml $BUG_BUDDY_DIR/bugzilla-config.xml)
(./create-mostfreq-xml.pl > temp.xml && iconv -c -f ASCII -t utf8 temp.xml > temp2.xml && rm temp.xml && mv temp2.xml $BUG_BUDDY_DIR/bugzilla-mostfreq.xml)
(cd $BUG_BUDDY_DIR && md5sum bugzilla-products.xml >  md5sums)
(cd $BUG_BUDDY_DIR && md5sum bugzilla-config.xml   >> md5sums)
(cd $BUG_BUDDY_DIR && md5sum bugzilla-mostfreq.xml >> md5sums)

## 2.0.x report
#wget http://bugzilla-test.gnome.org/reports/gnome-report.cgi?version=2.0 -qO temp2.0.html
#if  test -f temp2.0.html
#then mv temp2.0.html $REPORTS_DIR/gnome-20-report.html
#fi

# 2.2.x report
#wget http://bugzilla-test.gnome.org/reports/gnome-report.cgi?version=2.1/2.2 -qO temp2.2.html
#if  test -f temp2.2.html
#then mv temp2.2.html $REPORTS_DIR/gnome-22-report.html
#fi

# 2.4.x report
#wget http://bugzilla-test.gnome.org/reports/gnome-report.cgi?version=2.3/2.4 -qO temp2.4.html
#if  test -f temp2.4.html
#then mv temp2.4.html $REPORTS_DIR/gnome-24-report.html
#fi

(cd $BUGZILLA_DIR

## 2.6.x report
#QUERY_STRING="version=2.5/2.6" ./reports/gnome-report.cgi > $OUTPUT_DIR/temp2.6.html
#if  test -f $OUTPUT_DIR/temp2.6.html
#then mv $OUTPUT_DIR/temp2.6.html $REPORTS_DIR/gnome-26-report.html
#fi

# 2.8.x report
QUERY_STRING="version=2.7/2.8" ./reports/gnome-report.cgi > $OUTPUT_DIR/temp2.8.html
if  test -f $OUTPUT_DIR/temp2.8.html
then mv $OUTPUT_DIR/temp2.8.html $REPORTS_DIR/gnome-28-report.html
fi

# 2.10.x report
QUERY_STRING="version=2.9/2.10" ./reports/gnome-report.cgi > $OUTPUT_DIR/temp2.10.html
if  test -f $OUTPUT_DIR/temp2.10.html
then mv $OUTPUT_DIR/temp2.10.html $REPORTS_DIR/gnome-210-report.html
fi

# 2.12.x report
QUERY_STRING="version=2.11/2.12" ./reports/gnome-report.cgi > $OUTPUT_DIR/temp2.12.html
if  test -f $OUTPUT_DIR/temp2.12.html
then mv $OUTPUT_DIR/temp2.12.html $REPORTS_DIR/gnome-212-report.html
fi

# 2.14.x report
QUERY_STRING="version=2.13/2.14" ./reports/gnome-report.cgi > $OUTPUT_DIR/temp2.14.html
if  test -f $OUTPUT_DIR/temp2.14.html
then mv $OUTPUT_DIR/temp2.14.html $REPORTS_DIR/gnome-214-report.html
fi

## Cache the weekly bug summaries - Wayne Schuller (Nov 2002).
QUERY_STRING="" ./reports/weekly-bug-summary.cgi > $OUTPUT_DIR/weekly-bug-summary.html
mv $OUTPUT_DIR/weekly-bug-summary.html $REPORTS_DIR

## 2.0.x weekly bug summary
#wget http://bugzilla-test.gnome.org/reports/weekly-bug-summary.cgi?version=2.0 -qO weekly-bug-summary-gnome20.html
#mv $OUTPUT_DIR/weekly-bug-summary-gnome20.html $REPORTS_DIR

## 2.2.x weekly bug summary
#wget http://bugzilla-test.gnome.org/reports/weekly-bug-summary.cgi?version=2.1/2.2 -qO weekly-bug-summary-gnome22.html
#mv $OUTPUT_DIR/weekly-bug-summary-gnome22.html $REPORTS_DIR

## 2.4.x weekly bug summary
#wget http://bugzilla-test.gnome.org/reports/weekly-bug-summary.cgi?version=2.3/2.4 -qO weekly-bug-summary-gnome24.html
#mv $OUTPUT_DIR/weekly-bug-summary-gnome24.html $REPORTS_DIR

## 2.6.x weekly bug summary
#QUERY_STRING="version=2.5/2.6" ./reports/weekly-bug-summary.cgi > $OUTPUT_DIR/weekly-bug-summary-gnome26.html
#mv $OUTPUT_DIR/weekly-bug-summary-gnome26.html $REPORTS_DIR

# 2.8.x weekly bug summary
QUERY_STRING="version=2.7/2.8" ./reports/weekly-bug-summary.cgi > $OUTPUT_DIR/weekly-bug-summary-gnome28.html
mv $OUTPUT_DIR/weekly-bug-summary-gnome28.html $REPORTS_DIR

# 2.10.x weekly bug summary
QUERY_STRING="version=2.9/2.10" ./reports/weekly-bug-summary.cgi > $OUTPUT_DIR/weekly-bug-summary-gnome210.html
mv $OUTPUT_DIR/weekly-bug-summary-gnome210.html $REPORTS_DIR

# 2.10.x weekly bug summary
QUERY_STRING="version=2.11/2.12" ./reports/weekly-bug-summary.cgi > $OUTPUT_DIR/weekly-bug-summary-gnome212.html
mv $OUTPUT_DIR/weekly-bug-summary-gnome212.html $REPORTS_DIR

# 2.10.x weekly bug summary
QUERY_STRING="version=2.13/2.14" ./reports/weekly-bug-summary.cgi > $OUTPUT_DIR/weekly-bug-summary-gnome214.html
mv $OUTPUT_DIR/weekly-bug-summary-gnome214.html $REPORTS_DIR

# mostfrequent.cgi (this script uses lots of CPU)
QUERY_STRING="" ./reports/mostfrequent.cgi > $OUTPUT_DIR/mostfrequent.html
mv $OUTPUT_DIR/mostfrequent.html $REPORTS_DIR

)
