#!/usr/bin/perl -wT

use DBI;
use XML::Generator;
use strict;
use vars (qw[$db_host $db_port $db_user $db_pass]);

my $xml = XML::Generator->new('escape' => 'always',
			      'conformance' => 'strict',
			      'pretty' => 2
			      );

do '/home/admin/bugzilla/private.pl' or die "Can't read private-test.pl: $!";

my $drh = DBI->install_driver('mysql') or die "Can't connect to the database.";
my $connectstring = "dbi:mysql:bugs:host=$db_host:port=$db_port";
my $dbh = DBI->connect($connectstring, $db_user, $db_pass)
    or die "Can't connect to the table '$connectstring'.";

my $sth = $dbh->prepare("SELECT product,description,disallownew FROM products");
my $rv = $sth->execute;

my @products;

my $row;
while ($row = $sth->fetchrow_hashref) {
    my $product = $row->{product};

    # (by fherrera) l10n product is breaking this xml generation.
    # This is a temporal workaround
    next if $product eq 'l10n';

    # (by ovitters) gnome-backgrounds contain some background images.
    # It gets a lot of misfiled bugreports. As b.g.o and bug-buddy do not yet
    # allow attachments, it isn't useful
    next if $product eq 'gnome-backgrounds';

    my $cmph = $dbh->prepare("SELECT value,description FROM components WHERE program = '$product'");
    my $cmprv = $cmph->execute;

    my $component;
    my @component_xml;
    while ($component = $cmph->fetchrow_hashref) {
	push @component_xml, $xml->component({value		=> $component->{value},
					      description	=> $component->{description}});
    }

    my $versh = $dbh->prepare("SELECT value FROM versions WHERE program = '$product'");
    my $versrv = $versh->execute;

    my $version;
    my @version_xml;
    while ($version = $versh->fetchrow_hashref) {
	my @fields;

	push @fields, 'value' => $version->{value} if $version->{value} ne '';

	push @version_xml, $xml->version({value => $version->{value}});
    }

    push @products, $xml->product({name			=> $row->{product},
				   description		=> $row->{description},
				   disallownew		=> $row->{disallownew}},
				  @component_xml,
				  @version_xml
				  );
}

my $total = $xml->products(@products);

print qq[<!DOCTYPE products SYSTEM "http://bugzilla-test.gnome.org/bugzilla.dtd">\n];
print $total . "\n";

END { $dbh->disconnect if $dbh }


