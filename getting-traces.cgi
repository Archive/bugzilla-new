#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
use strict;

require "CGI.pl";

print "Location: http://live.gnome.org/GettingTraces\nContent-type: text/html\n\n";


print <<FIN;
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>getting-traces.cgi has moved</title>
</head><body>
<h1>Getting-traces.cgi has moved</h1>
<p>The document has moved <a href="http://live.gnome.org/GettingTraces">here</a>.</p>
<hr>
</body></html>
FIN

exit;
