#!/usr/bin/perl

# WARNING: The only time I tried to test, this script had a bug and
# worked halfway and I had to fix it up manually.  I fixed the bug and
# am pretty sure it works fine now, but be warned.

push @INC, "/usr/local/www/bugzilla/bugzilla";
chdir "/usr/local/www/bugzilla/bugzilla";

require "globals.pl";

ConnectToDatabase(1);
GetVersionTable();

my $version = $ARGV[0];
my $milestone = $ARGV[1];

if (!defined($version) || !defined ($milestone))
{
	print "Usage: $0 version milestone\n";
	print "  version   - new valid gnome version (gg_version)\n";
	print "  milestone - new valid gnome milestone (gg_milestone)\n\n";
	print "The program only allows adding both.  It does show you\n";
        print "the changes it will make and gives you a chance to back\n";
        print "out.  Note that the only time I tried to test, it had\n";
        print "a bug and I had to fix up manually.  I think it works\n";
        print "now, but be warned.\n\n";
	exit();
}

  $query = "
    SELECT
      name, valid_exp
    FROM
      customfields
    ";

  SendSQL ($query);

  my ($old_gg_version, $old_gg_milestone);
  while (my ($name, $valid_exp)=FetchSQLData()) {
    if ($name eq 'gg_version') {
      $old_gg_version = $valid_exp;
    } elsif ($name eq 'gg_milestone') {
      $old_gg_milestone = $valid_exp;
    }
  }

  print "Please verify:\n";
  print "  Chaning gg_version from\n" .
        "    $old_gg_version\n" .
        "  to\n" .
        "    $old_gg_version|$version\n";
  print "  Chaning gg_milestone from\n" .
        "    $old_gg_milestone\n" .
        "  to\n  " .
        "  $old_gg_milestone|$milestone\n";
  print "Is this correct? (Y/N): ";

  my $answer = <STDIN>;
  if ($answer !~ /^\s*[Yy]/) {
    $| = 1;  # Force stdout to flush immediately
    print "\nTough, making the change anyway!";
    sleep(1);
    print "...just kidding!\n";
    print "Script aborted.\n";
    exit();
  }

  $query = "
    UPDATE
      customfields
    SET
      valid_exp = '$old_gg_version|$version'
    WHERE
      name = 'gg_version'
    ";
  SendSQL ($query);

  $query = "
    UPDATE
      customfields
    SET
      valid_exp = '$old_gg_milestone|$milestone'
    WHERE
      name = 'gg_milestone'
    ";
  SendSQL ($query);

  unlink("data/versioncache");
  print "\ngg_version and gg_milestone updated.";
exit();
