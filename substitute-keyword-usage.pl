#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-

use diagnostics;
use strict;

use lib qw(.);

require "globals.pl";
#use vars qw($template $vars);

ConnectToDatabase(1);
#GetVersionTable();

  my $oldname = $ARGV[0];
  my $newname = $ARGV[1];
  my $loginname = $ARGV[2];

  if (!defined($oldname) || !defined($newname) || !defined($loginname)) {
    print "Usage: old_keyword new_keyword loginname_of_user_doing_change\n";
    exit();
  }

  # 1) Verify that the keywords exist
  SendSQL("SELECT id FROM keyworddefs WHERE name=" . SqlQuote($oldname));
  my ($old_keywordid) = FetchSQLData() or die "No such keyword: $oldname\n";
  SendSQL("SELECT id FROM keyworddefs WHERE name=" . SqlQuote($newname));
  my ($new_keywordid) = FetchSQLData() or die "No such keyword: $newname\n";

  # 2) Find $keyword_fieldid
  SendSQL("SELECT fieldid FROM fielddefs WHERE name = 'keywords'");
  my ($keyword_fieldid) = FetchSQLData();

  # 3) Find $userid
  SendSQL("SELECT userid FROM profiles 
           WHERE login_name = " . SqlQuote($loginname));
  my ($userid) = FetchSQLData() or die "No such user: $loginname\n";

  # 4) Get $curtime
  SendSQL("SELECT NOW()");
  my ($curtime) = FetchSQLData();

  # 5) Find the relevant bugs
  #  Possible alternative? :
  #    SELECT bug_id, bugs.keywords FROM bugs 
  #      WHERE INSTR(bugs.keywords, SqlQuote($oldname))
  my @sql_data = ();
  SendSQL("SELECT bugs.bug_id, bugs.keywords
           FROM bugs, keywords, keyworddefs
           WHERE keywords.bug_id=bugs.bug_id
             AND keyworddefs.id=keywords.keywordid
             AND keyworddefs.name=" . SqlQuote($oldname));
  while (my ($bug_id, $keywords) = FetchSQLData() ) {
    PushGlobalSQLState();

    # 6) Update bugs.keywords
    my $removed = SqlQuote($oldname);
    my $added = SqlQuote($newname);
    if ($keywords =~ /\b$newname\b/) {
      $keywords =~ s/\b$oldname\b//;
      $added = "''";
    } else {
      $keywords =~ s/\b$oldname\b/$newname/;
    }
    #print("  UPDATE bugs 
    #         SET delta_ts = delta_ts, keywords='$keywords'
    #         WHERE bug_id=$bug_id\n");
    SendSQL("UPDATE bugs 
             SET delta_ts = delta_ts, keywords='$keywords'
             WHERE bug_id=$bug_id");

    # 7) Record the update in bugs_activity
    #print("  INSERT INTO bugs_activity
    #         VALUES ($bug_id, $userid, '$curtime', $keyword_fieldid, 
    #                 $removed, $added, NULL)\n");
    SendSQL("INSERT INTO bugs_activity
             VALUES ($bug_id, $userid, '$curtime', $keyword_fieldid, 
                     $removed, $added, NULL)");

    # 8) Record the update in keywords
    if ($added ne "''") {
      #print("  UPDATE keywords 
      #         SET keywordid = $new_keywordid
      #         WHERE keywordid = $old_keywordid
      #         AND bug_id = $bug_id\n");
      SendSQL("UPDATE keywords 
               SET keywordid = $new_keywordid
               WHERE keywordid = $old_keywordid
               AND bug_id = $bug_id");
    } else {
      #print("  DELETE FROM keywords 
      #         WHERE keywordid = $old_keywordid 
      #           AND bug_id = $bug_id\n");
      SendSQL("DELETE FROM keywords 
               WHERE keywordid = $old_keywordid
                 AND bug_id = $bug_id");
    }

    PopGlobalSQLState();

    #print "Updated bug $bug_id\n";
    #last;
  }

  print "All references to keyword $oldname successfully changed " .
        "to $newname by $loginname.\n";
