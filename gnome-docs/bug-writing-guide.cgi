#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#

use strict;

chdir '..';
require "CGI.pl";
require "globals.pl";

print "Content-type: text/html\n\n";
#PutHeader ("Bug Writing HOWTO",'');

print <<FIN;
<H1 ALIGN="CENTER">Bug Writing HOWTO</H1>
<P ALIGN="CENTER"><B>(Maintained by <A HREF="mailto:bugmaster\@gnome.org">the GNOME Bugmasters.</A>)</B>
<DL><!- DL #1-!>
<DT><H3><B>Why You Should Read This</B></H3>

<DD>The <a href="http://www.gnome.org">GNOME</a> bug tracking system (<A HREF="http://bugzilla.gnome.org">Bugzilla</A>) allows any interested individuals on the Internet to directly report and track bugs in the GNOME Project.<BR>
	<BR>
	Like you, we want your bug reports to result in bug fixes; the more effectively a bug is reported, the more likely that a developer will actually fix it.<BR>
	<BR>
	By following these guidelines, you can help ensure that your bugs stay at the top of the developers' heap, and get fixed. The <A HREF="http://bugzilla.gnome.org/reports/simple-bug-guide.cgi">Simple Bug Guide</A>is the preferred method of submitting a bug - it incorporates parts of these guidelines.
</DL><!-DL #1-!>

<DL><!- DL #2-!>
<DT><H3><B>How to Enter your Useful Bug Report into Bugzilla</B>:</H3>

<DD>
<OL>
      <LI>Are you <U>sure</U> you don't want to use the <A HREF="http://bugzilla.gnome.org/reports/simple-bug-guide.cgi">Simple Bug Guide</A>? It is the easy way of doing this :) 
      <LI>Before you enter your bug, you need to make sure it has not been previously reported. Mozilla.org has a great tutorial on using the Bug Query interface to do this at <A HREF="http://www.mozilla.org/quality/help/beginning-duplicate-finding.html">http://www.mozilla.org/quality/help/beginning-duplicate-finding.html</A> that is quite applicable to our bugzilla.<BR>
	After searching, if you've discovered a new bug, report it in Bugzilla. Either use the <A HREF="http://bugzilla.gnome.org/reports/simple-bug-guide.cgi">Simple Bug Guide</A> (recommended) or follow the instructions below:
	<LI>OK, then. Click <a href="http://bugzilla.gnome.org/enter_bug.cgi">here</a> to open the bug reporting page. You'll probably want to keep this page open in a different window.
<LI>Select the product that you've found a bug in. If you don't see the program that you are using here, then it might not be a GNOME program: for example, Evolution bugs are tracked at <a href="http://bugzilla.ximian.com">Ximian</a>, Abiword bugs at <a href="http://bugzilla.abisource.com">abisource.com</a>, and XMMS bugs at <a href="http://bugzilla.xmms.org">xmms.org.</a> We also maintain <a href="other_products.html">a more complete list</a> of other bug tracking systems related to GNOME 
<LI>If you haven't logged into Bugzilla already, you'll need to enter your email address, password, and press the &quot;Login&quot; button. (If you don't yet have a password, leave the password text box empty, and press the &quot;email me a password&quot; button instead. You'll receive an email message with your password shortly.)
	</OL>
<BR>
Now, fill out the form. Here's what each part of the form means:
</DL><!-DL #5N1-!>

</DL><!-DL #4-!>

<DL><!- DL #6-!>
<DD>
	<P><B>Product: In which product did you find the bug?</B><BR>
		You just filled this out on the last page.

	<P><B>Version: In which product version did you find the bug?</B><BR>
		Many of our products do not yet use this field. If there are options, please select the appropriate one. Otherwise, please make sure to enter the version of the product in the comments of the bug.

	<P><B>Component: In which part of the program does the bug exist?</B><BR>
		This should (in most cases) be self-explanatory. If you are unsure, pick one that seems as close as possible to the one that you feel would be correct. 

	<P><B>OS: On which Operating System (OS) did you find this bug?</B><BR>
		This is fairly straightforward. What operating system were you using when you ran into this bug? If you don't see your OS listed, select 'other' and then give us more details in the 'OS Details' line. If you know that this occurs on all platforms, please try to select 'all.'

	<P><B>Severity: How bad is the bug?</B><BR>
		The full list of severities is <a href="http://bugzilla.gnome.org/bug_status.html#severity">here</a>, but in a nutshell: if it crashes the application, it should be Critical, and if it is a new feature request, it should be Wishlist. Everything else will fall in somewhere in between. 

	<P><B>Priority: How important is the bug?</B><BR>
		The full list of priorities is <a href="http://bugzilla.gnome.org/bug_status.html#priority">here</a>, but in a nutshell: Does it happen a lot or in a big, key portion of the application? Or is it particularly embarassing? Then it is High. Urgent and Immediate should be reserved for things that block builds, or otherwise make an app fairly useless. These may also be used for things that are big build goals.

	<P><B>Assigned To: Which developer should be responsible for fixing this bug?</B><BR>
		Bugzilla will automatically assign the bug to a default developer upon submitting a bug report; the text box exists to allow you to manually assign it to a different developer. (To see the list of default developers for each component, click on the Component link.)

	<P><B>Cc: Who else should receive e-mail updates on changes to this bug? </B><BR>
		List the full e-mail addresses of other individuals who should receive an e-mail update upon every change to the bug report. You can enter as many e-mail addresses as you'd like; e-mail addresses must be separated by commas, with no spaces between the addresses. Note two things: first, you always receive updates, so there is no need to add yourself here; and second: the addresses must be accounts in bugzilla.<BR>
		<BR>
		You would not normally change either this or assigned to: from their default values.

	<P><B>URL: Is there a URL with more data about the bug?</B><BR>
		This can be useful for posting logs, screenshots, cores, and other things that might be useful to someone trying to debug the problem, but which might not be appropriate to attach to the bug because of number or size.

	<P><B>Summary: How would you describe the bug, in approximately 60 or fewer characters?</B><BR>
		A good summary should <U>quickly and uniquely identify a bug report</U>. Otherwise, developers cannot meaningfully query by bug summary, and will often fail to pay attention to your bug report when reviewing a 10 page bug list. Think of it as a &quot;title&quot;. Good subject lines are specific and do not just say &quot;Bug&quot; or &quot;It crashed&quot;. &quot;Crash when I try to save&quot; or &quot;Forgets my theme preferences&quot; are much better. 


	<P><B>Description: What can you tell the developer about this bug? </B><BR>
		Please provide as detailed of a problem diagnosis in this field as possible, including as much as possible of the following information. Feel free to cut and paste the bold headers directly into your bug as a guideline for the data we need.
</DL><!-DL #10N1-!>
<BR>
<DL><!- DL #11N1-!><DD>
	<DL><!- DL #12N2-!>
		<DD><B>Overview Description:</B> More detailed expansion of summary.<P>

		<DL><!- DL #13N3-!><DD>
			<SMALL><PRE>Dragging a folder from Nautilus to my desktop crashes Nautilus.</PRE></SMALL>
		</DL><!-DL #13N3-!>

		<DD><B>Steps to Reproduce: </B>The minimal set of steps necessary to trigger the bug. Include any special setup steps.<P>

		<DL><!- DL #14N3-!><DD>
			<SMALL><PRE>1) Open my home directory in Nautilus.
2) Try to drag it to the desktop.
3) This only happens for the directory named !$#!$#!\$ and not the directory named 'steve'.</PRE></SMALL>
	</DL><!-DL #12N2-!>

		<DD><B>Actual Results:</B> What the application did after performing the above steps.<P>

		<DL><!- DL #15N2-!><DD>
			<SMALL><PRE>Nautilus crashed, and when it restarted, the folder was not on my desktop.</PRE></SMALL>
		</DL><!-DL #15N2-!>

		<DD><B>Expected Results:</B> What the application should have done, were the bug not present.<P>

		<DL><!- DL #16N2-!><DD>
			<SMALL><PRE>The folder !$#!$#!\$ should have been on the desktop.</PRE></SMALL>
		</DL><!-DL #16N2-!>

		<DD><B>Additional Builds and Platforms:</B> Whether or not the bug takes place on other distributions, and (if you've seen it before) how long this bug has been taking place..<P>

		<DL><!- DL #18N2-!><DD>
			<SMALL><PRE> - Also occurs on Debian stable and Red Hat 6.2.
 - Has occurred since the evo nightly on June 6th.</PRE></SMALL>
		</DL><!-DL #18N2-!>

		<DD><B>Additional Information:</B> Anything else you think might be relevant. 


</DL><!-DL #6-!>

	<P>You're done! <BR>
	<BR>
	After double-checking your entries for any possible errors, press the &quot;Commit&quot; button, and your bug report will be part of the Bugzilla database.<BR>
	<I><P>	</I>
</DL><!-DL #19-!>


<TABLE BORDER="1" CELLPADDING="10" CELLSPACING="0" BGCOLOR="#FEFFE6">
	<TR>
		<TD>

The GNOME Bug Writing HOWTO was originally written by Luis Villa. It is heavily based on the <a href="http://www.mozilla.org/quality/bug-writing-guidelines.html">Mozilla QA Bug Writing Guidelines</a>, originally written by <A HREF="mailto:eli\@prometheus-music.com">Eli Goldberg</A> with help from Claudius Gayle, Jan Leger, Peter Mock, Chris Pratt, Chris Yeh and Felix Miata. Text was also borrowed from the Help Documents for Gnome Bug-Buddy, written by Telsa Gwynne and Jacob Berkman. Additional <A HREF="mailto:bugmaster\@gnome.org">suggestions</A> welcome.)

		</TD>
	</TR>
</TABLE>
</TD>
</TR>
</TABLE>
FIN
#PutFooter();
exit;
