#!/usr/bonsaitools/bin/perl -wT
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# weekly-bug-summary.cgi
#
# Display some nice bug stats for the Gnome weekly summary.
#
# Some of the functions can be passed the HTML or XML option.
# We use XML for emailing the Gnome weekly summary guys.
#
# This file was based on the original mostfrequent.cgi but has been
# modified GREATLY by Wayne Schuller (k_wayne@linuxpower.org), Jan 2002.
#
# TODO
#	- Some products have way too many bugs. Break it down via component.
#	- Use percentages for the diff figures.

use diagnostics;
use strict;
use lib ".";

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "weekly-summary-utils.pl";

# Output appropriate HTTP response headers
print "Content-type: text/html\n";
# Changing attachment to inline to resolve 46897 - zach@zachlipton.com
print "Content-disposition: inline; filename=bugzilla_report.html\n\n";

ConnectToDatabase(1);
# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
quietly_check_login();

# make sensible defaults.
my $version = "";               # Don't limit to "2.7/2.8" or "2.5/2.6", etc.
my $days = 7; 			# Change this if defn of week changes.
my $products = 15; 		# Show the top 15 products.
my $hunters = 15; 		# Show the top 15 hunters 
my $reporters = 15; 		# Show the top 15 reporters 
my $keyword = "%";		# Don't limit to any keyword.
my $links = "yes";		# Create links (can get very large sometimes)

if (defined $::FORM{'days'} && $::FORM{'days'} ne ""){
	$days = $::FORM{'days'};
	detaint_natural($days) || die "days parameter must be a number";
}

if (defined $::FORM{'products'} && $::FORM{'products'} ne ""){
	$products = $::FORM{'products'};
	detaint_natural($products) || die "products parameter must be a number";
}

if (defined $::FORM{'hunters'} && $::FORM{'hunters'} ne ""){
	$hunters = $::FORM{'hunters'};
	detaint_natural($hunters) || die "hunters parameter must be a number";
}

if (defined $::FORM{'reporters'} && $::FORM{'reporters'} ne ""){
	$reporters = $::FORM{'reporters'};
	detaint_natural($reporters) || die "reporters parameter must be a number";
}

if (defined $::FORM{'links'}){
	$links = $::FORM{'links'};
}

PutHeader("Gnome Bugzilla Weekly Summary");
if (defined $::FORM{'KEYWORD'}){
	$keyword = $::FORM{'KEYWORD'};
}

if (defined $::FORM{'version'} && $::FORM{'version'} ne ""){
        $version = $::FORM{'version'};
}


# Always spew informative messages.
print "<center><h1>Weekly Bug Summary";
if ($version) {
  print " for " . html_quote($version);
}
print "</h1></center>";
print "<p>Some weekly bug summary info for <a href=\"http://bugzilla.gnome.org\">bugzilla.gnome.org</a>.";
print " Last updated: " . time2str("%a %b %e %T %Z %Y", time(), "UTC")  . ".";

if (defined $::FORM{'KEYWORD'}){
	print "<p> (This page is limited to bugs matching the $::FORM{'KEYWORD'} keyword.)";
}

# Run the bugs_opened function, which should also create the buglist URL.
our $buglist;
my $bugs_opened = &bugs_opened("%", $days, $keyword, $version);

print "<h3> Total Reports: " . &print_total_bugs_on_bugzilla($keyword, $version) . " (";

if ($links eq "yes") {
	print "<a href=\"$buglist\">$bugs_opened</a> reports opened and ";
} else {
	print "$bugs_opened reports opened and ";
}

# Run the bugs_closed function, which should also create the buglist URL.
my $bugs_closed = &bugs_closed("%", $days, $keyword, $version);

if ($links eq "yes") {
	print "<a href=\"$buglist\">$bugs_closed</a> reports closed";
} else {
	print "$bugs_closed reports closed";
}

print " in the last $days days. Including enhancement requests)</h2>\n";

print "<h3>Top $products Gnome modules with the most reports:</h3>";
print "<p>(the second column excludes enhancement requests, the other columns cover all reports)</p>";
&print_product_bug_lists($products, $days, "HTML", \*STDOUT, $keyword, $links, $version);

print "<h3>Top $hunters people who closed the most bugs in the last $days days:</h3>";
&print_bug_hunters_list($hunters, $days, "HTML", \*STDOUT, $keyword, $links, $version);

print "<h3>Top $reporters people who reported the most bugs in the last $days days:</h3>";
&print_bug_reporters_list($reporters, $days, "HTML", \*STDOUT, $keyword, $links, $version);

print "<p> This script (but not the html version) can be called with the KEYWORD or VERSION parameter. This allows you to pass arbitrary keyword constraints.</p>";

print "<p> If you spot any errors in this page please report it to <a href=\"mailto:bugmaster\@gnome.org\">bugmaster\@gnome.org</a>. Thanks.</p>";

PutFooter();
