#!/usr/bonsaitools/bin/perl -w
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# g2-progress-utils.pl [borrowed from something by Wayne Schuller]
#
# four simple functions to get numbers for studying gnome2 progress
# incoming_bugs()- takes $days and a [product|component]. Must pass $days
#  but if product/component is not passed defaults to all.
# outgoing_fixes()- takes $days, $devel (either a developers email or
#  a substring, like 'ximian'), and $product. $days is mandatory but if 
#  devel or product is empty it acts like 'all.'
# outgoing_nonfix_resolutions()- as outgoing_fixes but for dups, invalids, etc.
# outstanding_bugs()- takes $product; if $product is empty it acts like 'all.'
# multiprod_outstanding_bugs()- takes @products and returns a total.

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";

ConnectToDatabase(1);

sub incoming_bugs() {
    my ($days, $product) = @_;
    my $query;
    
    $query = <<FIN;
select 
    count(distinct bugs.bug_id)
from 
    bugs
where
    to_days(bugs.creation_ts) >= (to_days(now()) - $days)
and
   (
    bugs.product = '$product'
    or
    bugs.component = '$product'
    )
and
    bugs.keywords LIKE '%GNOME2%'
FIN

    SendSQL ($query);
    return (FetchOneColumn());
}

sub outgoing_fixes() {
    my ($days, $devel, $product) = @_;
    my $query;
    
    $query = <<FIN;
select 
    bugs.bug_id
from 
    bugs, bugs_activity, profiles
where
    to_days(bugs_activity.bug_when) >= (to_days(now()) - $days)
and
    (
     bugs_activity.newvalue = 'RESOLVED'
     )   
and
    bugs_activity.bug_id = bugs.bug_id
and
    bugs.resolution = 'FIXED'  
and
    profiles.userid = bugs_activity.who
and
    profiles.login_name = '$devel'	
and
   (
    bugs.product = '$product'
    or
    bugs.component = '$product'
    )
and
    bugs.keywords LIKE '%GNOME2%'
FIN

    SendSQL ($query);
    my @bugs;
    my $bug_count;
    while (my $bug = FetchSQLData())
    {
	$bug_count++;
	push @bugs, $bug;
    }
    return ($bug_count,@bugs);
}

sub outgoing_nonfix_resolutions() {
    my ($days, $devel, $product) = @_;
    my $query;
    
    $query = <<FIN;
select 
    bugs.bug_id
from 
    bugs, bugs_activity, profiles
where
    to_days(bugs_activity.bug_when) >= (to_days(now()) - $days)
and
    (
     bugs_activity.newvalue = 'RESOLVED'
     )   
and
    bugs_activity.bug_id = bugs.bug_id
and
    bugs.resolution != 'FIXED'  
and
    profiles.userid = bugs_activity.who
and
    profiles.login_name = '$devel'	
and
   (
    bugs.product = '$product'
    or
    bugs.component = '$product'
    )
and
    bugs.keywords LIKE '%GNOME2%'
FIN

    SendSQL ($query);
    my @bugs;
    my $bug_count;
    while (my $bug = FetchSQLData())
    {
	$bug_count++;
	push @bugs, $bug;
    }
    return ($bug_count,@bugs);
}

sub outstanding_bugs() {
    my ($product) = @_;
    my $query;
    
    $query = <<FIN;

select 
    count(distinct bugs.bug_id)
from 
    bugs
where
   (
    bugs.product = '$product'
    or
    bugs.component = '$product'
    )
and
    (
     bugs.priority = 'Immediate'
     or
     bugs.priority = 'Urgent'
     or
     bugs.priority = 'High'
     )
and
    (
     bugs.bug_status = 'NEW'
     or
     bugs.bug_status = 'ASSIGNED'
     or
     bugs.bug_status = 'REOPENED'
    )
and
    bugs.keywords LIKE '%GNOME2%'
and
    bugs.keywords LIKE '%triaged%'
FIN

    SendSQL ($query);
    return (FetchOneColumn());
}

sub multiprod_outstanding_bugs() {
    if (defined @_){
    my (@products) = @_;
    my $total = 0;
    foreach my $prod (@products)
    {
	my $tmp = &outstanding_bugs($prod);
	$total += $tmp;
    }
    return $total;
}
    else
    {
	return 'NA';
    }
}

sub multiprod_incoming_bugs() {
    if (defined @_){
    my ($days,@products) = @_;
    my $total = 0;
    foreach my $prod (@products)
    {
	my $tmp = &incoming_bugs($days,$prod);
	$total += $tmp;
    }
    return $total;
}
    else
    {
	return 'NA';
    }
}

sub outgoing_fixes_over_span() {
    my ($start_date, $stop_date, $devel) = @_;
    my $query;
    
    $query = <<FIN;
select 
    bugs.bug_id
from 
    bugs, bugs_activity, profiles
where
    priority != 'NORMAL'
and 
    priority != 'LOW'
and
    to_days(bugs_activity.bug_when) >= (to_days('$start_date'))
and
    to_days(bugs_activity.bug_when) <= (to_days('$stop_date'))
and
    (
     bugs_activity.newvalue = 'RESOLVED'
     )   
and
    bugs_activity.bug_id = bugs.bug_id
and
    profiles.userid = bugs_activity.who
and
    profiles.login_name = '$devel'
and
    product != 'gnumeric'
and
    product != 'bug-buddy'
and
    bugs.resolution = 'FIXED'  
and
    bugs.keywords LIKE '%GNOME2%'
FIN

    SendSQL ($query);
    my $buglist;
    my $bug_count;
    while (my $bug_id = FetchSQLData())
    {
	$bug_count++;
        $buglist .= $bug_id;
        $buglist .= ',';
    }
    return ($bug_count,$buglist);
}
