#!/usr/bonsaitools/bin/perl -w

# (c) Copyright Elijah Newren 2005
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use diagnostics;
use strict;

  # Bugzilla doesn't expect us to be running in a reports subdirectory, so we
  # do a little magic to trick it.
  use Cwd;
  my $dir = cwd;
  if ($dir =~ /reports$/) {
    chdir "..";
  }
  push @INC, "reports/."; # many scripts now are in the reports subdirectory
   
  require "CGI.pl";
  use vars qw(%FORM); # globals from CGI.pl

  require "globals.pl";
  require "patch-report-utils.pl";

  #
  # First, get all the variables needed
  #
  my $quoted_product="'%'";   # (string) which product to search, % for all
  my $quoted_component="'%'"; # (string) which component to search, % for all
  my $patch_status="none";  # (string) status of patches to search for
  my $min_days = -1;    # (int) Don't show patches younger than this (in days)
  my $max_days = -1;    # (int) Don't show patches older than this (in days)
  my $submitter;        # (int) submitter id

  my %F;
  my %M;
  ParseUrlString($::buffer, \%F, \%M);

  foreach my $field (keys %F) {
      if (scalar(@{$M{$field}}) == 0) {
	next;
      }
      if ($field eq "product") {
        $quoted_product = join(',', map(SqlQuote($_), @{$M{$field}}));
      } elsif ($field eq "component") {
        $quoted_component = join(',', map(SqlQuote($_), @{$M{$field}}));
      }
  }
  
  if (defined $::FORM{'patch-status'} && $::FORM{'patch-status'} ne ""){
    $patch_status = $::FORM{'patch-status'};
  }
  if (defined $::FORM{'min_days'} && $::FORM{'min_days'} ne ""){
    $min_days = $::FORM{'min_days'};
    detaint_natural($min_days) || die "min_days parameter must be a number";
  }
  if (defined $::FORM{'max_days'} && $::FORM{'max_days'} ne ""){
    $max_days = $::FORM{'max_days'};
    detaint_natural($max_days) || die "max_days parameter must be a number";
  }
  if (exists $::FORM{'submitter'} && $::FORM{'submitter'} ne "") {
    $submitter = DBNameToIdAndCheck($::FORM{'submitter'});
  }

  # Determine the report type...
  my $type;
  if ($patch_status eq "none") {
    $type = "Unreviewed";
  } elsif ($patch_status eq "obsolete") {
    $type = "Obsolete";
  } else {
    my $tempquery = "
      SELECT id
      FROM attachstatusdefs
      WHERE name = " . SqlQuote($patch_status);
    SendSQL($tempquery);
    my ($id) = FetchSQLData();
    if (defined($id)) {
      $type = $patch_status;
    } else {
	DisplayError("Invalid patch status: $patch_status\n") && exit;
    }
  }

  #
  # Second, set the report printing up...
  #

  # Output appropriate HTTP response headers
  print "Content-type: text/html\n";
  # Changing attachment to inline to resolve 46897 - zach@zachlipton.com
  print "Content-disposition: inline; filename=patch-report.cgi\n\n";

  ConnectToDatabase(1);
  # When we show the footer, it's confusing to claim the user isn't logged in
  # if they are.
  quietly_check_login();

  PutHeader("Patch Report");
  print "
    <center>
    <h1>$type Patches</h1>
    </center>
    ";

  print "\n";

  #
  # Third, collect the needed information
  #
  my $stats = get_unreviewed_patches_and_stats($quoted_product, 
                                               $quoted_component,
                                               $patch_status,
                                               $min_days,
                                               $max_days,
                                               $submitter);

  #
  # Finally, print it all out
  #

  ## Obsolete, txt (instead of html) method:
  #foreach my $product (@{$prod_list}) {
  #  print "$product->{name}\n";
  #  foreach my $component (@{$product->{component_list}}) {
  #    print "  $component->{name}\n";
  #    print "  $component->{maintainer}\n";
  #    foreach my $bug (@{$component->{bug_list}}) {
  #      print "    http://bugzilla.gnome.org/show_bug.cgi?id=$bug->{id}\n";
  #      print "    $bug->{summary}\n";
  #      print "    $bug->{priority}/$bug->{severity}\n";
  #      foreach my $patch (@{$bug->{patch_list}}) {
  #        print "      http://bugzilla.gnome.org/attachment.cgi?" .
  #              "id=$patch->{id}&action=edit\n";
  #        print "      $patch->{description}\n";
  #      }
  #    }
  #  }
  #}

  # Print it all out in table-like fashion
  foreach my $product (@{$stats->{product_list}}) {
    print "<h3>$product->{name}<a name=\"$product->{name}\"></a> " .
          "($product->{count})</h3>\n";
    print "<ul>\n";
    foreach my $component (@{$product->{component_list}}) {
      print "<li>Component: $component->{name} ($component->{count})</li>\n";
      $component->{maintainer} =~ s/[@.]/\ /g;
      #print "<li>$component->{maintainer}</li>\n";
      print "<ul>\n";
      foreach my $bug (@{$component->{bug_list}}) {
        print "<li>Bug <a href=\"http://bugzilla.gnome.org/show_bug.cgi?" .
              "id=$bug->{id}\">$bug->{id}</a>: $bug->{summary}" .
              " ($bug->{priority}/$bug->{severity})</li>\n";
        print "<ul>\n";
        foreach my $patch (@{$bug->{patch_list}}) {
          print "<li><i>Attachment $patch->{id}</i> ($patch->{age} days) " .
                "<a href=\"http://bugzilla.gnome.org/attachment.cgi?" .
                "id=$patch->{id}&action=edit\">$patch->{description}</a>" .
                "</li>\n";
        }
        print "</ul>\n";
      }
      print "</ul>\n";
    }
    print "</ul>\n";
  }
  if ($stats->{count} > 0) {
    print "<p>$stats->{count} " . lc($type) . " patches found.</p>\n";
  } else {
    print "<p>No patches found matching your criteria.</p>\n";
  }

  #
  # Don't forget to provide some useful information...
  #
  print "<!--" .
    "<p> SOME USEFUL PARAMETERS TO PASS THIS SCRIPT: <ul>\n" .
    "<li>product   (Only show unreviewed patches in this product)</li>\n" .
    "<li>component (Only show unreviewed patches in this component)</li>\n" .
    "<li>min_days  (Don't show patches younger than this)</li>\n" .
    "<li>max_days  (Don't show patches older than this)</li>\n" .
    "</ul></p>\n" .
    "-->\n";

  PutFooter();
