#!/usr/bonsaitools/bin/perl -w
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# weekly-summary-utils.pl
#
# Utility library shared between weekly-bug-summary.cgi and email_gnome_summary_report.pl and others probably.
#
# Some of the functions can be passed the HTML or XML option.
# We use XML for emailing the Gnome weekly summary guys.
#
# This file was based on the original mostfrequent.cgi but has been
# modified GREATLY by Wayne Schuller (k_wayne@linuxpower.org), Jan 2002.
#
# TODO
#	- Some products have way too many bugs. Break it down via component.
#	- Use percentages for the diff figures.

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";

ConnectToDatabase(1);

sub print_total_bugs_on_bugzilla() {
	my($keyword, $version) = @_;
	my $query;

        if ($version) {
          my $quoted_version = SqlQuote ($version);
          $query = <<FIN;
select
	bugs.bug_id
from 
	bugs, bugs_customfields, customfields
where
    ( customfields.name = 'gg_version'
      AND customfields.id = bugs_customfields.cf_id
      AND bugs.bug_id = bugs_customfields.bug_id
      AND bugs_customfields.value = $quoted_version
    )
and
FIN
        } else {
	$query = <<FIN;
select
	bugs.bug_id
from 
	bugs
where 
FIN
        }

	$query .= <<FIN;
	(bugs.bug_status = 'NEW' or bugs.bug_status = 'ASSIGNED' or bugs.bug_status = 'REOPENED' or bugs.bug_status = 'UNCONFIRMED')
and 
	bugs.keywords LIKE '$keyword'

FIN

	SendSQL ($query);
	my $count = 0;
	# YES I know SQL can count results for me, but I take my arithmetic
	# very seriously.
	while (my ($bugs) = FetchSQLData()) {
		$count++;	
	}
	return($count);
}

# bugs_closed
# Show how many bugs have been closed for $product in $days.
# Pass "%" as product to get all products.
sub bugs_closed() {
	my($product, $days, $keyword, $version) = @_;
	my $query = "";

	# Warning suppresion.
	if (!defined $product) { $product = "%"; }
	if (!defined $days) { $days = "7"; }
	if (!defined $keyword) { $keyword = "%"; }

	# We are going to build a long SQL query.
        if ($version) {
          my $quoted_version = SqlQuote ($version);
          $query = <<FIN;
select
	bugs.bug_id
from 
	bugs, bugs_customfields, customfields, bugs_activity
where
    ( customfields.name = 'gg_version'
      AND customfields.id = bugs_customfields.cf_id
      AND bugs.bug_id = bugs_customfields.bug_id
      AND bugs_customfields.value = $quoted_version
    )
and
FIN
        } else {
	$query = <<FIN;
select
	bugs.bug_id
from 
	bugs, bugs_activity
where 
FIN
        }

        $query .= <<FIN;
        (bugs.bug_status='RESOLVED' or bugs.bug_status='CLOSED')
and
	(bugs_activity.added='RESOLVED' or bugs_activity.added='CLOSED')
and 
        bugs_activity.bug_when >= FROM_DAYS(TO_DAYS(NOW())-$days)
and
	bugs.bug_id = bugs_activity.bug_id
and
	bugs.product LIKE '$product'
and 
	bugs.keywords LIKE '$keyword'

group by bugs.bug_id

FIN

	SendSQL ($query);

	# Create URL of all the bugs that match this function.
	our $buglist = "http://bugzilla.gnome.org/buglist.cgi?bug_id=";

	# YES I know SQL can count results for me, but I take my arithmetic
	# very seriously.
	my $count = 0;
	while (my ($bug_id) = FetchSQLData()) {
		$count++;	
		$buglist .= $bug_id;
		$buglist .= ',';
	}

	return($count);
}

# bugs_opened
# Show how many bugs have been opened for $product in $days.
# Pass "%" as product to get all products.
sub bugs_opened() {
	my($product, $days, $keyword, $version) = @_;

	my $query = "";

	# Warning suppresion.
	if (!defined $product) { $product = "%"; }
	if (!defined $days) { $days = "7"; }
	if (!defined $keyword) { $keyword = "%"; }
	
	# We are going to build a long SQL query.
        if ($version) {
          my $quoted_version = SqlQuote ($version);
          $query = <<FIN;
select
	bugs.bug_id
from 
	bugs, bugs_customfields, customfields
where
    ( customfields.name = 'gg_version'
      AND customfields.id = bugs_customfields.cf_id
      AND bugs.bug_id = bugs_customfields.bug_id
      AND bugs_customfields.value = $quoted_version
    )
and 
FIN
        } else {
	  $query = <<FIN;
select
	bugs.bug_id
from 
	bugs
where 
FIN
        }

        # Unfortunately, some bugs are posted from machines with timestamps
        # in the future.  Usually, we don't want to count these bugs, but
        # if the user is doing really large searches then perhaps we do...
        if ($days < 365) {
          $query .= "
              bugs.creation_ts <= FROM_DAYS(TO_DAYS(NOW())+1)
            AND
            ";
        }

        $query .= <<FIN;
        bugs.creation_ts >= FROM_DAYS(TO_DAYS(NOW())-$days)
and
	bugs.product LIKE '$product'
and 
	bugs.keywords LIKE '$keyword'
FIN

        $query .= "group by bugs.bug_id";

	SendSQL ($query);

	# Create URL of all the bugs that match this function.
	our $buglist = "http://bugzilla.gnome.org/buglist.cgi?bug_id=";

	# YES I know SQL can count results for me, but I take my arithmetic
	# very seriously.
	# Not only count the bugs, but keep a copy of each bug number in the global
	# variable 'buglist' which is a URL linking to the bugs.
	my $count = 0;
	while (my ($bug_id) = FetchSQLData()) {
		$count++;	
		$buglist .= $bug_id;
		$buglist .= ',';
	}

	return($count);
}

# $format can be HTML or XML
sub print_product_bug_lists() {
	my($number, $days, $format, $fh, $keyword, $links, $version) = @_;

	my $query;

	# We are going to build a long SQL query.
        if ($version) {
          my $quoted_version = SqlQuote ($version);
          $query = <<FIN;
select
	bugs.product, count(bugs.product), count(bugs.product) as n
from 
	bugs, bugs_customfields, customfields
where
    ( customfields.name = 'gg_version'
      AND customfields.id = bugs_customfields.cf_id
      AND bugs.bug_id = bugs_customfields.bug_id
      AND bugs_customfields.value = $quoted_version
    )
and
FIN
        } else {
	$query = <<FIN;
select
	bugs.product, count(bugs.product), count(bugs.product) as n 
from 
	bugs
where 
FIN
        }

        $query .= <<FIN;
	(bugs.bug_status = 'NEW' or bugs.bug_status = 'ASSIGNED' or bugs.bug_status = 'REOPENED' or bugs.bug_status = 'UNCONFIRMED')
and
	bugs.bug_severity != 'enhancement'
and 
	bugs.keywords LIKE '$keyword'

group by product
order by n desc 
limit $number
FIN

	# For each product we want to show the difference in the last period.
	# But this will involve two sql connections at once, which the bugzilla
	# functions don't handle too nicely.
	# So lets collect the data first and then print the table.
	my %product_count;

	SendSQL ($query);
	while (my ($product, $count, $n) = FetchSQLData()) {
		$product_count{$product} = $count;	
	}

	# Print a nice cross-referenced table of results in HTML.
	if ($format eq "HTML") {
	print "<table border=1 cellspacing=0 cellpadding=5>\n";
	print "<tr><th>Product</th><th>Currently OPEN Bugs</th><th>Opened in last $days days</th><th>Closed in last $days days</th><th>Change</th></tr>\n";
	}

	foreach my $product (reverse sort 
			{$product_count{$a} <=> $product_count{$b}}
					keys (%product_count)) {

				
		our $buglist; # Global var used to contain URL of bug lists.

		my $opened = &bugs_opened($product, $days, $keyword, $version);
		my $openlist = $buglist; # Remember the URL of open bug lists.

		my $closed = &bugs_closed($product, $days, $keyword, $version);
		my $closedlist = $buglist; # Remember the URL of closed bug lists.

		my $change = $opened-$closed;

	if ($format eq "HTML") {

		# Build the link for the open bugs - everything but enhancement requests.
		# First we need to replace any + signs with the code %2B
		# This is especially needed for links with gtk+ in them.
		my $newproduct = $product;
		$newproduct =~ s/\+/\%2B/;

		# This link has to match the SQL query above.
		my $link = "http://bugzilla.gnome.org/buglist.cgi?product=$newproduct&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=blocker&bug_severity=critical&bug_severity=major&bug_severity=normal&bug_severity=minor&bug_severity=trivial&gg_version=$version";

		# Set a different background color for positive or negative.
		my $bgcolor = "";
		if( $change > 0 ) {
			$bgcolor = " bgcolor=\"#ffc849\"";
		} elsif( $change <= 0 ) {
			$bgcolor = " bgcolor=\"#00d51d\"";
		}

		if ($links eq "yes") {
			print <<FIN;
        			<tr>
				<td><a href="$link">$product</a></td>
				<td align="right"><a href="$link">$product_count{$product}</a></td>
				<td align="right"><a href="$openlist">+$opened</a></td>
				<td align="right"><a href="$closedlist">-$closed</a></td>
				<td align="right" $bgcolor>$change</td>
				</tr>
FIN
		} else {
			print <<FIN;
        			<tr>
				<td>$product</td>
				<td align="right">$product_count{$product}</td>
				<td align="right">+$opened</td>
				<td align="right">-$closed</td>
				<td align="right" $bgcolor>$change</td>
				</tr>
FIN
		}
	} else {
		# XML version
		print $fh "<bug-item open='$product_count{$product}' opened='$opened' closed='$closed'>$product</bug-item>\n";
	}

	}

	if ($format eq "HTML") { 
		print "</table><p>\n"; 
	} else {
		print $fh "</bug-modules>\n"; 
	}
}

sub print_bug_hunters_list() {
	my($number, $days, $format, $fh, $keyword, $links, $version) = @_;

	my $query;

	# We are going to build a long SQL query.
        if ($version) {
          my $quoted_version = SqlQuote ($version);
          $query = <<FIN;
select
	assign.login_name, count(assign.login_name), count(assign.login_name) as n 
from 
	bugs, bugs_customfields, customfields, bugs_activity, profiles assign
where
    ( customfields.name = 'gg_version'
      AND customfields.id = bugs_customfields.cf_id
      AND bugs.bug_id = bugs_customfields.bug_id
      AND bugs_customfields.value = $quoted_version
    )
and
FIN
        } else {
	$query = <<FIN;
select
	assign.login_name, count(assign.login_name), count(assign.login_name) as n 
from 
	bugs, bugs_activity, profiles assign
where 
FIN
        }

        $query .= <<FIN;
	(bugs_activity.added='RESOLVED' or bugs_activity.added='CLOSED')
and 
        bugs_activity.bug_when >= FROM_DAYS(TO_DAYS(NOW())-$days)
and 
	bugs_activity.who = assign.userid
and
	bugs.bug_id = bugs_activity.bug_id
and 
        (bugs.bug_status='RESOLVED' or bugs.bug_status='CLOSED')
and
	bugs.keywords LIKE '$keyword'

group by assign.login_name 
order by n desc 
limit $number

FIN

	# We want to give links to the bugs the hunter has closed.
	# But this will involve two sql connections at once, which the bugzilla
	# functions don't handle too nicely.
	# So lets collect the data first and then print the table.
	my %hunterlist;

	SendSQL ($query);
	while (my ($user, $count, $n) = FetchSQLData()) {
		$hunterlist{$user} = $count;	
	}	

	if ($format eq "HTML") {
		# Print a nice cross-referenced table of results.
		print "<table border=1 cellspacing=0 cellpadding=5>\n";
		print "<tr><th>Position</th><th>Bugzilla User</th><th>Number of Bugs Resolved</th></tr>\n";
	} else {
		print $fh "<bug-hunters>\n";
	}

	my $position = 1;  # Mark the positions in the html table.

	foreach my $user (reverse sort 
			{$hunterlist{$a} <=> $hunterlist{$b}}
					keys (%hunterlist)) {

		# defang the email address
		my $defang_user = $user;
		$defang_user =~ y/\@\./  /;

		if ($format eq "HTML") {
			if ($links eq "yes") {
				my $buglist = &get_hunter_bugs($user, $days, $keyword, $version);

				print <<FIN;
	        			<tr>
					<td>$position</td>
					<td>$defang_user</td>
					<td align="right"><a href=$buglist>$hunterlist{$user}</a></td>
					</tr>
FIN
			} else {
				print <<FIN;
	        			<tr>
					<td>$position</td>
					<td>$defang_user</td>
					<td align="right">$hunterlist{$user}</td>
					</tr>
FIN
			}

		$position++;
		} else {
		# XML version
		print $fh "<bug-item closed='$hunterlist{$user}'>$defang_user</bug-item>\n";
		}
	}

	if ($format eq "HTML") {
		print "</table><p>\n";
	} else {
		print $fh "</bug-hunters>\n";
	}
}

sub get_hunter_bugs() {
	my($user, $days, $keyword, $version) = @_;

	my $query;

	# We are going to build a long SQL query.
	# We are going to build a long SQL query.
        if ($version) {
          my $quoted_version = SqlQuote ($version);
          $query = <<FIN;
select
	bugs.bug_id
from 
	bugs, bugs_customfields, customfields, bugs_activity, profiles assign
where
    ( customfields.name = 'gg_version'
      AND customfields.id = bugs_customfields.cf_id
      AND bugs.bug_id = bugs_customfields.bug_id
      AND bugs_customfields.value = $quoted_version
    )
and
FIN
        } else {
	$query = <<FIN;
select
	bugs.bug_id
from 
	bugs, bugs_activity, profiles assign
where 
FIN
        }

        $query .= <<FIN;
	assign.login_name = "$user"
and
	(bugs_activity.added='RESOLVED' or bugs_activity.added='CLOSED')
and 
        bugs_activity.bug_when >= FROM_DAYS(TO_DAYS(NOW())-$days)
and 
	bugs_activity.who = assign.userid
and
	bugs.bug_id = bugs_activity.bug_id
and 
        (bugs.bug_status='RESOLVED' or bugs.bug_status='CLOSED')
and 
	bugs.keywords LIKE '$keyword'

FIN
	
	my $buglist = "http://bugzilla.gnome.org/buglist.cgi?bug_id=";

	SendSQL ($query);
	while (my ($bug_id) = FetchSQLData()) {
		$buglist .= $bug_id;
		$buglist .= ',';
	}

	return ($buglist);
}

sub print_bug_reporters_list() {
	# This is based upon print_bug_hunters_list.
	my($number, $days, $format, $fh, $keyword, $links, $version) = @_;

	my $query;

	# We are going to build a long SQL query.
        if ($version) {
          my $quoted_version = SqlQuote ($version);
          $query = <<FIN;
select
	assign.login_name, count(assign.login_name), count(assign.login_name) as n 
from 
	bugs, bugs_customfields, customfields, profiles assign
where
    ( customfields.name = 'gg_version'
      AND customfields.id = bugs_customfields.cf_id
      AND bugs.bug_id = bugs_customfields.bug_id
      AND bugs_customfields.value = $quoted_version
    )
and
FIN
        } else {
	$query = <<FIN;
select
	assign.login_name, count(assign.login_name), count(assign.login_name) as n 
from 
	bugs, profiles assign
where 
FIN
        }

        $query .= <<FIN;
        bugs.creation_ts >= FROM_DAYS(TO_DAYS(NOW())-$days)
and 
	bugs.reporter = assign.userid
and
	bugs.keywords LIKE '$keyword'

group by assign.login_name 
order by n desc 
limit $number

FIN

	# We want to give links to the bugs the reporter has closed.
	# But this will involve two sql connections at once, which the bugzilla
	# functions don't handle too nicely.
	# So lets collect the data first and then print the table.
	my %hunterlist;

	SendSQL ($query);
	while (my ($user, $count, $n) = FetchSQLData()) {
		$hunterlist{$user} = $count;	
	}	

	if ($format eq "HTML") {
		# Print a nice cross-referenced table of results.
		print "<table border=1 cellspacing=0 cellpadding=5>\n";
		print "<tr><th>Position</th><th>Bugzilla User</th><th>Number of Bugs Reported</th></tr>\n";
	} else {
		print $fh "<bug-reporters>\n";
	}

	my $position = 1;  # Mark the positions in the html table.

	foreach my $user (reverse sort 
			{$hunterlist{$a} <=> $hunterlist{$b}}
					keys (%hunterlist)) {

		# defang the email address
		my $defang_user = $user;
		if ($user eq 'unknown@gnome.bugs') {
			$defang_user = "-bug buddy submission-";
		} elsif ($user eq 'debbugs-export@gnome.bugs') {
			$defang_user = "-Debian bug export-";
                } elsif ($user eq 'bugbuddy-import@ximian.com') {
                        $defang_user = "-Ximian bug-buddy submission-";
		} else {
			$defang_user =~ y/\@\./  /;
		}


		if ($format eq "HTML") {
			if ($links eq "yes") {
				my $buglist = &get_reporter_bugs($user, $days, $keyword, $version);

				print <<FIN;
	        			<tr>
					<td>$position</td>
					<td>$defang_user</td>
					<td align="right"><a href=$buglist>$hunterlist{$user}</a></td>
					</tr>
FIN
			} else {
				print <<FIN;
	        			<tr>
					<td>$position</td>
					<td>$defang_user</td>
					<td align="right">$hunterlist{$user}</td>
					</tr>
FIN
			}

		$position++;
		} else {
		# XML version
		print $fh "<bug-item closed='$hunterlist{$user}'>$defang_user</bug-item>\n";
		}
	}

	if ($format eq "HTML") {
		print "</table><p>\n";
	} else {
		print $fh "</bug-reporters>\n";
	}
}

sub get_reporter_bugs() {
	my($user, $days, $keyword, $version) = @_;

	my $query;

	# We are going to build a long SQL query.
	# We are going to build a long SQL query.
        if ($version) {
          my $quoted_version = SqlQuote ($version);
          $query = <<FIN;
select
	bugs.bug_id
from 
	bugs, bugs_customfields, customfields, profiles assign
where
    ( customfields.name = 'gg_version'
      AND customfields.id = bugs_customfields.cf_id
      AND bugs.bug_id = bugs_customfields.bug_id
      AND bugs_customfields.value = $quoted_version
    )
and
FIN
        } else {
	$query = <<FIN;
select
	bugs.bug_id
from 
	bugs, profiles assign
where 
FIN
        }

        $query .= <<FIN;
	assign.login_name = "$user"
and 
        bugs.creation_ts >= FROM_DAYS(TO_DAYS(NOW())-$days)
and 
	bugs.reporter = assign.userid
and 
	bugs.keywords LIKE '$keyword'

FIN
	
	my $buglist = "http://bugzilla.gnome.org/buglist.cgi?bug_id=";

	SendSQL ($query);
	while (my ($bug_id) = FetchSQLData()) {
		$buglist .= $bug_id;
		$buglist .= ',';
	}

	return ($buglist);
}

1;
