#!/usr/bonsaitools/bin/perl -w

# (c) Copyright Elijah Newren 2005
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use diagnostics;
use strict;

  require "CGI.pl";
  use vars qw(%FORM); # globals from CGI.pl

  require "globals.pl";
  ConnectToDatabase(1);

sub get_unreviewed_patches_and_stats() {
  my ($quoted_product, $quoted_component, $patch_status,
      $min_days, $max_days, $submitter) = (@_);

  my $query;

  $query = "
    SELECT
      attachments.attach_id, attachments.bug_id,
      ( TO_DAYS(NOW()) - TO_DAYS(attachments.creation_ts) ) AS age,
      substring(attachments.description, 1, 70),
      bugs.product, bugs.component
    FROM
      attachments, bugs
    LEFT JOIN
      attachstatuses ON attachments.attach_id=attachstatuses.attach_id
    WHERE
        attachments.bug_id = bugs.bug_id
    ";
  if ($quoted_product ne "'%'") {
    $query .= "
      AND
        bugs.product IN ($quoted_product)";
  }
  if ($quoted_component ne "'%'") {
    $query .= "
      AND
        bugs.component IN ($quoted_component)";
  }
  if ($submitter) {
    # $submitter is a numeric
    $query .= "
      AND
        attachments.submitter_id = $submitter";
  }
  if ($min_days != -1) {
    $query .= "
      AND
        TO_DAYS(NOW()) - TO_DAYS(attachments.creation_ts) >= $min_days";
  }
  if ($max_days != -1) {
    $query .= "
      AND
        TO_DAYS(NOW()) - TO_DAYS(attachments.creation_ts) <= $max_days";
  }
  $query .= "
      AND
        attachments.ispatch = '1'";
  if ($patch_status eq 'obsolete') {
    $query .= "
      AND
        attachments.isobsolete  = '1'";
  } else {
    $query .= "
      AND
        attachments.isobsolete != '1'";
  }
  if ($patch_status eq 'none') {
    $query .= "      
      AND
        attachstatuses.statusid IS  NULL";
  } elsif ($patch_status ne 'obsolete') {
    my $tempquery = "
      SELECT id
      FROM attachstatusdefs
      WHERE name = " . SqlQuote($patch_status);
    SendSQL($tempquery);
    my ($id) = FetchSQLData();
    $query .= "
      AND
        attachstatuses.statusid = " . SqlQuote($id);
  }
  $query .= "
      AND
      (
        (
          bugs.bug_status = 'unconfirmed'
        OR
          bugs.bug_status = 'new'
        OR
          bugs.bug_status = 'assigned'
        OR
          bugs.bug_status = 'reopened'
        )
      )
    ORDER BY
      bugs.product, bugs.component, attachments.bug_id, attachments.attach_id
    ";

  SendSQL ($query);

  my ($cur_product, $cur_component, $cur_bug) = ('', '', 0);
  my ($prod_list, $comp_list, $bug_list, $patch_list) = ([], [], [], []);
  my ($new_product, $new_component);
  my ($total_count, $prod_count, $comp_count) = (0, 0, 0);
  my $stats = {
    'count' => 0,
    'product_list' => []
    };
  $prod_list = $stats->{product_list};  
  while (my ($attach_id, $bug_id, $age, $desc, $prod, $comp)=FetchSQLData()) {

    # Check if we've moved on to a new product
    if ($cur_product ne $prod) {
      if ($cur_product ne '') {
        $new_product->{count} = $prod_count;
        $prod_count = 0;
        $new_component->{count} = $comp_count;
        $comp_count = 0;
      }
      $cur_product = $prod;
      $new_product = {
        'name' => $prod,
        'component_list' => []
        };
      push @{$prod_list}, $new_product;
      $cur_component = '';
      $comp_list = $new_product->{component_list};
    }
    
    # Check if we've moved on to a new component
    if ($cur_component ne $comp) {
      if ($cur_component ne '') {
        $new_component->{count} = $comp_count;
        $comp_count = 0;
      }
      $cur_component = $comp;
      my $quoted_prod = SqlQuote($cur_product);
      my $quoted_comp = SqlQuote($cur_component);
      PushGlobalSQLState();
      $query = "
        SELECT
          initialowner
        FROM
          components
        WHERE
            program = $quoted_prod
          AND
            value   = $quoted_comp
        ";
      SendSQL ($query);
      my ($maintainer_id) = FetchSQLData();
      $query = "
        SELECT
          login_name
        FROM
          profiles
        WHERE
          userid = $maintainer_id
        ";
      SendSQL ($query);
      my ($maintainer) = FetchSQLData();
      PopGlobalSQLState();

      $new_component = {
        'name' => $comp,
        'maintainer' => $maintainer,
        'bug_list' => []
        };
      push @{$comp_list}, $new_component;
      $cur_bug = 0;
      $bug_list = $new_component->{bug_list};
    }
    
    # Check if we've moved on to a new component
    if ($cur_bug ne $bug_id) {
      $cur_bug = $bug_id;
      PushGlobalSQLState();
      $query = "
        SELECT
          substring(short_desc, 1, 70), priority, bug_severity
        FROM
          bugs
        WHERE
          bug_id = $bug_id
        ";
      SendSQL ($query);
      my ($bug_desc, $priority, $severity) = FetchSQLData();
      PopGlobalSQLState();

      my $new_bug = {
        'id' => $bug_id,
        'summary' => $bug_desc,
        'priority' => $priority,
        'severity' => $severity,
        'patch_list' => []
        };
      push @{$bug_list}, $new_bug;
      $patch_list = $new_bug->{patch_list};
    }

    my $new_patch = {
      'id' => $attach_id,
      'age' => $age,
      'description' => $desc
      };
    push @{$patch_list}, $new_patch;

    $total_count++;
    $prod_count++;
    $comp_count++;

    # printf "%6d %6d %s %s %s\n", $attach_id, $bug_id, $prod, $comp, $desc;
  }

  # Update the counts for the final product and component, as well as the total
  $stats->{count} = $total_count;
  $new_product->{count} = $prod_count;
  $new_component->{count} = $comp_count;

  return $stats;
}

# Let perl know we ended okay
1;
