#!/usr/bonsaitools/bin/perl -w
#
# Copyright 2005, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

# TODO (outdated; may still have useful info)
#   Things to consider:
#     date marked as easy fix
#     whether it has patches
#     whether it has unreviewed or accepted patches
#     number of comments since the bug was marked as easy fix
#     what was the comment that accompanied the keyword being added
#   Formats:
#     just a big html page with all the above info in a table?
#     some kind of blog-like thing that Bryan was suggesting?
#   Other points to ponder:
#     Ways to fight misuse of keyword (lart people who misuse it, and
#       maybe even make a query that points out possible misuses, by
#       perhaps searching bugs_activity.who and seeing if their marked
#       as a gnome hacker--or checking the wording of the comment somehow)

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";

use POSIX qw(ceil floor);

ConnectToDatabase(1);

sub get_keyword_bug_info() {
  my ($only_core_gnome, $product, $keyword) = (@_);

  my $quoted_product = SqlQuote($product);
  my $quoted_keyword = SqlQuote($keyword);

  #
  # First, get the fieldid of 'keywords'
  #
  my $query = "
    SELECT
      fieldid, name
    FROM
      fielddefs
    WHERE
      name = 'keywords'
    ";
  SendSQL ($query);
  my ($keywords_fieldid) = FetchSQLData();

  $query = "
    SELECT
      bugs.bug_id, 
      bugs.creation_ts,
      TO_DAYS(NOW())-TO_DAYS(bugs.creation_ts),
      bugs.reporter
    FROM
      bugs";
  if ($only_core_gnome) {
    $query .= "
      LEFT JOIN
        products ON bugs.product = products.product";
  }
  $query .= "
    WHERE
      INSTR(bugs.keywords,$quoted_keyword)
    AND
      (
       bugs.bug_status='UNCONFIRMED' OR
       bugs.bug_status='NEW' OR
       bugs.bug_status='ASSIGNED' OR
       bugs.bug_status='REOPENED'
      )
    AND
      bugs.product LIKE $quoted_product";
  if ($only_core_gnome) {
    $query .= "
      AND
      products.isgnome = 1";
  }

  SendSQL ($query);

  my @temp_data = ();
  my @data = ();

  while (my ($bug_id, $bug_creation, $bug_age, $bug_who) = FetchSQLData()) {
    push @temp_data, { id       => $bug_id, 
                       creation => $bug_creation,
                       age      => $bug_age,
                       who      => $bug_who };
  }

  foreach my $bug (@temp_data) {
    #
    # First, try to find the last time the keyword was added
    #
    my $when = $bug->{creation};
    my $age  = $bug->{age};
    my $who  = $bug->{who};
    $query = "
      SELECT
        bugs_activity.bug_when,
        TO_DAYS(NOW())-TO_DAYS(bugs_activity.bug_when),
        bugs_activity.who
      FROM
        bugs_activity
      WHERE
        bugs_activity.bug_id = $bug->{id}
      AND
        bugs_activity.fieldid = $keywords_fieldid
      AND
        INSTR(bugs_activity.added,$quoted_keyword)
      ORDER BY
        bugs_activity.bug_when DESC
      LIMIT
        1";
    SendSQL ($query);
    my ($change_when, $change_age, $change_who) = FetchSQLData();
    if ($change_when && $change_age < $age) {
      $age = $change_age;
      $when = $change_when;
      $who  = $change_who;
    }

    #
    # Second, find the number of comments since the bug had the
    # keyword added
    #
    $query = "
      SELECT
        count(longdescs.bug_id)
      FROM
        longdescs
      WHERE
        longdescs.bug_id = $bug->{id}
      AND
        longdescs.bug_when > '$when'";
    SendSQL ($query);
    my ($comment_count) = FetchSQLData();

    #
    # Third, find the number of attachments that the bug has
    #
    $query = "
      SELECT
        count(attachments.bug_id)
      FROM
        attachments
      WHERE
        attachments.bug_id = $bug->{id}";
    # AND attachments.creation_ts > '$when'";  # only if after marked?
    SendSQL ($query);
    my ($patch_count) = FetchSQLData();

    #
    # Fourth, find the comment that was added at the same time the bug was
    # marked with the keyword, if any
    #
    $query = "
      SELECT
        substring(longdescs.thetext, 1, 5000)
      FROM
        longdescs
      WHERE
        longdescs.bug_id = $bug->{id}
      AND
        longdescs.bug_when = '$when'";
    SendSQL ($query);
    my ($comment_when_marked) = FetchSQLData();
    if (!defined($comment_when_marked)) {
      $comment_when_marked = "-No comment made when the " . $quoted_keyword .
                             " keyword was added-";
    }

    #
    # Fifth, get all other fields we want/need
    #
    $query = "
      SELECT
        product, substring(bugs.short_desc, 1, 60)
      FROM
        bugs
      WHERE
        bugs.bug_id = $bug->{id}";
    SendSQL ($query);
    my ($bug_product, $desc) = FetchSQLData();

    my $query = "
      SELECT
        realname
      FROM
        profiles
      WHERE
        profiles.userid = $who";
    SendSQL ($query);
    my ($realname) = FetchSQLData();
    if (!$realname) {
      $query = "
        SELECT
          login_name
        FROM
          profiles
        WHERE
          profiles.userid = $who";
      SendSQL ($query);
      ($realname) = FetchSQLData();
      # Defang any email addys
      $realname =~ s/[@.]/\ /g;
    }

    push @data, { id            => $bug->{id},
                  product       => $bug_product,
                  description   => $desc,
                  comment_count => $comment_count,
                  patch_count   => $patch_count,
                  age           => $age,
                  comment       => $comment_when_marked,
                  who           => $realname };
  }

  return @data;
}

# Rip-off of quoteUrls from globals.pl with some minor changes.
sub quoteUrlsOutsideBug {
    my ($bug_id, $text) = (@_);
    return $text unless $text;
    
    my $base = Param('urlbase');

    my $protocol = join '|',
    qw(afs cid ftp gopher http https mid news nntp prospero telnet wais);

    my $count = 0;

    # Now, quote any "#" characters so they won't confuse stuff later
    $text =~ s/#/%#/g;

    # Next, find anything that looks like a URL or an email address and
    # pull them out the the text, replacing them with a "##<digits>##
    # marker, and writing them into an array.  All this confusion is
    # necessary so that we don't match on something we've already replaced,
    # which can happen if you do multiple s///g operations.

    my @things;
    while ($text =~ s%((mailto:)?([\w\.\-\+\=]+\@[\w\-]+(?:\.[\w\-]+)+)\b|
                    (\b((?:$protocol):[^ \t\n<>"]+[\w/])))%"##$count##"%exo) {
        my $item = $&;

        $item = value_quote($item);

        if ($item !~ m/^$protocol:/o && $item !~ /^mailto:/) {
            # We must have grabbed this one because it looks like an email
            # address.
            $item = qq{<A HREF="mailto:$item">$item</A>};
        } else {
            $item = qq{<A HREF="$item">$item</A>};
        }

        $things[$count++] = $item;
    }
    # Either a comment string or no comma and a compulsory #.
    while ($text =~ s/\bbug(\s|%\#)*(\d+),?\s*comment\s*(\s|%\#)(\d+)/"##$count##"/ei) {
        my $item = $&;
        my $bugnum = $2;
        $bugnum = $bug_id unless $bugnum;
        my $comnum = $4;
        $item = GetBugLink($bugnum, $item);
        $item =~ s/(id=\d+)/$1#c$comnum/;
        $things[$count++] = $item;
    }
    while ($text =~ s/\bcomment(\s|%\#)*(\d+)/"##$count##"/ei) {
        my $item = $&;
        my $num = $2;
        $item = value_quote($item);
        $item = qq{<A HREF="http://bugzilla.gnome.org/show_bug.cgi?id=$bug_id#c$num">$item</A>};
        $things[$count++] = $item;
    }
    while ($text =~ s/\bbug(\s|%\#)*(\d+)/"##$count##"/ei) {
        my $item = $&;
        my $num = $2;
        $item = GetBugLink($num, $item);
        $things[$count++] = $item;
    }
    while ($text =~ s/\b(Created an )?attachment(\s|%\#)*(\(id=)?(\d+)\)?/"##$count##"/ei) {
        my $item = $&;
        my $num = $4;
        $item = value_quote($item); # Not really necessary, since we know
                                    # there's no special chars in it.
        $item = qq{<a href="attachment.cgi?id=$num&amp;action=view">$item</a>};
        $things[$count++] = $item;
    }
    while ($text =~ s/\*\*\* This bug has been marked as a duplicate of (\d+) \*\*\*/"##$count##"/ei) {
        my $item = $&;
        my $num = $1;
        my $bug_link;
        $bug_link = GetBugLink($num, $num);
        $item =~ s@\d+@$bug_link@;
        $things[$count++] = $item;
    }

    $text = value_quote($text);
    $text =~ s/\&#013;/\n/g;

    # Stuff everything back from the array.
    for (my $i=0 ; $i<$count ; $i++) {
        $text =~ s/##$i##/$things[$i]/e;
    }

    # And undo the quoting of "#" characters.
    $text =~ s/%#/#/g;

    return $text;
}

# Let perl know we ended okay
1;
