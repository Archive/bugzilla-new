#!/usr/bonsaitools/bin/perl -w
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "g2-progress-utils.pl";

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

ConnectToDatabase(1);

PutHeader("GNOME2 Bug Report");

# If they didn't tell us a time period we choose the last day.
my $days = 1; 
my $devel;
my $product;

if (defined $::FORM{'days'}){
	$days = $::FORM{'days'};
}

if (defined $::FORM{'devel'}){
    $devel = $::FORM{'devel'};
}

if (defined $::FORM{'product'}){
    $product = $::FORM{'product'};
}


# Always spew informative messages.
print "bugs report for past ".$days." days for ".$devel." in ".$product."<BR>";
print "bugs fixed:".&outgoing_fixes($days,$devel,$product)."<BR>";
print "bugs otherwise resolved:".&outgoing_nonfix_resolutions($days,$devel,$product)."<BR>";
print "bugs incoming:".&incoming_bugs($days,$product)."<BR>";
print "bugs open:".&outstanding_bugs($product)."<BR>";

PutFooter();
