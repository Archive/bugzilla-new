#!/usr/bonsaitools/bin/perl -w
#
# Copyright 2004, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";

ConnectToDatabase(1);

sub get_recent_mostfrequent_table() {
  my ($days, $only_core_gnome, $print_warnings) = @_;

  #
  # First, get the fieldid of 'resolution'
  #
  my $query = "
    SELECT
      fieldid
    FROM
      fielddefs
    WHERE
      name = 'resolution'
    ";
  SendSQL ($query);
  my ($bug_status_fieldid) = FetchSQLData();

  #
  # Now, the killer query to get all the bugs which are duplicates, with the
  # info of which bugs they are duplicates of
  #
  $query = "
    SELECT
      distinct(bugs.bug_id),
      duplicates.dupe_of
    FROM
      bugs, bugs_activity, duplicates
    ";
  if ($only_core_gnome) {
    $query .= "
      LEFT JOIN
        products ON bugs.product = products.product";
  }
  $query .= "  
    WHERE
      duplicates.dupe = bugs.bug_id
    AND
      bugs_activity.bug_id = bugs.bug_id
    AND
      bugs_activity.fieldid = $bug_status_fieldid
    AND
      bugs_activity.added = 'DUPLICATE'
    AND
      TO_DAYS(NOW()) - TO_DAYS(bugs_activity.bug_when) <= $days
    ";
  if ($only_core_gnome) {
    $query .= "
      and
      products.isgnome = 1";
  }
  $query .= "
    ORDER BY
      duplicates.dupe_of, bugs.bug_id";
  SendSQL ($query);

  #
  # Start filling a huge table (@the_data_yo), each row of which contains
  #   bug#, duplicate count, duplicate list
  #
  my @the_data_yo;
  my ($cur_bug, $dupe_list, $dupe_count, $total_dupes) = (-1, '', 0, 0);

  while (my ($bug_id, $dupe_of) = FetchSQLData()) {

    # Check if we've moved on to a new product and need to reset loop
    # counter-like variables
    if ($cur_bug != $dupe_of) {

      # If this isn't the very first product, we probably need to save
      # information for the last product we were gathering information
      # for.
      if ($cur_bug != -1) {
        push @the_data_yo, [$cur_bug, $dupe_count, $dupe_list];
      }

      # Reset the data for the new product
      $cur_bug = $dupe_of;
      $dupe_count = 0;
      $dupe_list      = "http://bugzilla.gnome.org/buglist.cgi?bug_id=";
    }

    $dupe_list .= "$bug_id,";
    $dupe_count++;
    $total_dupes++;
  }

  # Get the last product too
  push @the_data_yo, [$cur_bug, $dupe_count, $dupe_list];

  #
  # Now, find bugs having duplicates which themselves are a duplicate.  For
  # those which are, change the bug# in @the_data_yo table to the bug# of
  # which they are a duplicate.  (Well, except that bug may be a duplicate too,
  # so follow the chain...)
  #
  foreach my $listref (@the_data_yo) {
    my @dupelist;
    my $query = "
      SELECT bugs.bug_status, bugs.resolution
      FROM   bugs
      WHERE  bugs.bug_id = $listref->[0]
      ";
    SendSQL($query);
    my ($status, $resolution) = FetchSQLData();
    @dupelist = ($listref->[0]);
    while (($status eq "RESOLVED" ||
            $status eq "VERIFIED") &&
           $resolution eq "DUPLICATE") {
      my $query = "
        SELECT bugs.bug_id, duplicates.dupe_of
        FROM   bugs, duplicates
        WHERE
          bugs.bug_id = $listref->[0]
        AND
          duplicates.dupe = bugs.bug_id
        ";
      SendSQL($query);
      my ($origbug, $newbug) = FetchSQLData();

      # Watch out for inconsistent database--do this before trying to use
      # $newbug
      if (!defined($newbug)) {
        if ($print_warnings) {
          print "<!-- Warning: $listref->[0] is a dupe but has no entry in " .
                "duplicates!! -->\n";
        }
        last;
      }

      # Check for circularity in duplicates
      if (grep {$_ == $newbug} @dupelist) {
        if ($print_warnings) {
          print "<p><b><i>Warning!!!</b></i>" .
                " Circular duplicates found in database: ";
          foreach my $dupe (@dupelist) {
            print "$dupe, ";
          }
          print "$newbug</p>\n";
        }
        last;
      }
      push @dupelist, $newbug;

      $listref->[0] = $newbug;
      $query = "
        SELECT bugs.bug_status, bugs.resolution
        FROM   bugs
        WHERE  bugs.bug_id = $listref->[0]
        ";
      SendSQL($query);
      ($status, $resolution) = FetchSQLData();
    }
  }

  #
  # After the above, @the_data_yo can have multiple rows with the same bug#;
  # we want to combine those--we make a new table, @new_data, with this info
  #
  my @new_data;
  ($cur_bug, $dupe_count, $dupe_list) = (-1, 0, '');
  foreach my $listref (sort {$a->[0] <=> $b->[0]} @the_data_yo) {
    if ($listref->[0] != $cur_bug) {
      if ($cur_bug != -1) {
        push @new_data, [$cur_bug, $dupe_count, $dupe_list];
      }
      $cur_bug = $listref->[0];
      $dupe_count = $listref->[1];
      $dupe_list = $listref->[2];
    } else {
      $dupe_count += $listref->[1];
      my ($new_dupes) = $listref->[2] =~ /bug_id=([0-9,]+)/;
      $dupe_list .= $new_dupes;
    }
  }
  push @new_data, [$cur_bug, $dupe_count, $dupe_list];

  return @new_data;
}

# required, don't remove
1;
