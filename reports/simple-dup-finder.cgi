#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
#TODO:
#if we don't get a result, we can try a different tack
#when we upgrade to 2.16, this should also display what numbers something is a dup of it is a dup
#needs to ignore some functions like glog*

#BUGS
#does weird things with traces for things like 86835
#83904 is a dup of 78700 but not vice-versa

#SPECIAL CASES
#84883: stop at end of Thread 0, where Thread 0 has only 'sigaction' and 'main'
#'no symbol table info available'

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";

#set up the page
ConnectToDatabase(1);
GetVersionTable();

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

my $bug_id = $::FORM{'bug_id'};

PutHeader("possible CRASH dups",'');

print <<FIN;
USE: simple-dup-finder.cgi?bug_id=BUGNUMBER<br>
BUGNUMBER is a bug with a stack trace- this doesn't work for non-crash bugs, and probably never will. :)<br>
This page is <i>extremely</i> alpha right now. It is:
<ul>
<li>probably inaccurate
<li>definitely too strict
<li>probably too slow
<li>doesn't handle any special cases
</ul>
That said, it's been useful to me :) If you have problems, suggestions, whatever... please, please add a comment to <a href="../show_bug.cgi?id=87927">bug 87927</a> or email bugmaster\@gnome.org if you don't like bugzilla. I'm most interested in cases where it you know a duplicate exists and note that this page doesn't show it- any examples like that can only make the algorithm better. But other suggestions are welcome as well. <p>
First five function calls in the bug:<p>
FIN

my $query = <<FIN;
select
    thetext
from   longdescs
where
    bug_id='$bug_id'
and
    (
    thetext LIKE '%signal handler called%'
     or
    thetext LIKE '%sigaction%'
     or
    thetext LIKE '%killpg%'
 )
FIN

    print "<ol>";
# End build up $query string
SendSQL ($query);
my @lines;
my $start_switch=0;
my %functions;
while (my ($text) = FetchSQLData()) {
 @lines=split(/\n/,$text);
 foreach my $line (@lines)
 {
  #here, we try to ignore things before <signal handler> and/or killpg()
  if (($line=~/<signal handler called>|killpg|sigaction/) && ($start_switch==0)){
     $start_switch=1;
   }
  #here, we get only function names after the <signal handler>|killpg
  elsif (($line=~/^\#\d+ +0x[0-9A-Fa-f]+ in (\w+) \(/ ) && ($start_switch>=1)){
     print '<li>'.$1.'<br>';
     $functions{$start_switch} = $1;
     $start_switch++;
 }
  if ($start_switch>5){
      last;
  }
}
}
print"</ol>";

#There weren't any functions found, which is probably an error :) 
if($functions{1} eq ''){
print<<FIN;
No stack symbols were found in bug $bug_id. This may represent a bug in this page; if you believe there is a stack trace on the page, please email the bug number $bug_id to bugmaster\@gnome.org, or add a comment to <a href="../show_bug.cgi?id=87927">bug 87927.</a>.
FIN
exit;
}

print<<FIN;
<p>
Bugs that contain all five function calls:<p>
FIN

$query = <<FIN;
SELECT 
    bugs.bug_id, substring(bugs.bug_status,1,4), substring(bugs.resolution,1,4), substring(bugs.short_desc, 1, 60) 
FROM 
    bugs, longdescs
WHERE 
longdescs.bug_id = bugs.bug_id 
AND (INSTR(LOWER(longdescs.thetext),LOWER('$functions{1}'))
AND INSTR(LOWER(longdescs.thetext),LOWER('$functions{2}'))
AND INSTR(LOWER(longdescs.thetext),LOWER('$functions{3}'))
AND INSTR(LOWER(longdescs.thetext),LOWER('$functions{4}'))
AND INSTR(LOWER(longdescs.thetext),LOWER('$functions{5}')))
ORDER BY bugs.bug_status, bugs.bug_id
FIN



#debug
#print<<FIN;
#<code>
#$query
#</code><p>
#FIN

#should really do this counter better
    print "<ul>";
    my $crap_counter=0;
SendSQL ($query);
while (my ($id,$sta,$res,$des) = FetchSQLData()) {
print<<FIN;
<li><a href="../show_bug.cgi?id=$id">$id</a> $sta $des<br>
FIN
    $crap_counter++;
}
print "</ul>";
if($crap_counter!=0){
print<<FIN;
<p>
All bugs with valid strack traces should report <i>at least</i> themselves as duplicates of themselves. If they don't, that's a bug; please add a comment to <a href="../show_bug.cgi?id=87927">bug 87927</a> or email bugmaster\@gnome.org with the bug number that does <i>not</i> report itself as a duplicate of itself.
FIN
}
PutFooter();
