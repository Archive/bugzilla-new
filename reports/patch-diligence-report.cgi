#!/usr/bonsaitools/bin/perl -w
#
# Copyright 2004, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product); # globals from er, globals.pl

# Output appropriate HTTP response headers
print "Content-type: text/html\n";
# Changing attachment to inline to resolve 46897 - zach@zachlipton.com
print "Content-disposition: inline; filename=patch-diligence-report.html\n\n";

ConnectToDatabase(1);
# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
quietly_check_login();

# make sensible defaults.
my $min_days = 7;      # Don't show patches younger than this
my $max_days = 180;    # Don't show patches older than this
my $cutoff = 4;        # Don't list products with fewer patches than this
my $use_blocked = 0;   # (boolean) whether BLOCKED_BY_FREEZE counts as review
my $count_obsolete =0; # (boolean) whether to include obsolete patches in count
my $only_core_gnome=0; # (boolean) whether to only show core gnome products

if (defined $::FORM{'min_days'} && $::FORM{'min_days'} ne ""){
  $min_days = $::FORM{'min_days'};
  detaint_natural($min_days) || die "min_days parameter must be a number";
}

if (defined $::FORM{'max_days'} && $::FORM{'max_days'} ne ""){
  $max_days = $::FORM{'max_days'};
  detaint_natural($max_days) || die "max_days parameter must be a number";
}

if (defined $::FORM{'cutoff'} && $::FORM{'cutoff'} ne ""){
  $cutoff = $::FORM{'cutoff'};
}

if (defined $::FORM{'use_blocked'} && $::FORM{'use_blocked'} ne ""){
  $use_blocked = $::FORM{'use_blocked'};
}

if (defined $::FORM{'count_obsolete'} && $::FORM{'count_obsolete'} ne ""){
  $count_obsolete = $::FORM{'count_obsolete'};
}

if (defined $::FORM{'only_core_gnome'} && $::FORM{'only_core_gnome'} ne ""){
  $only_core_gnome = $::FORM{'only_core_gnome'};
}

PutHeader("Patch Diligence Report");

# Always spew informative messages.
print "<center><h1>Patch Diligence Report</h1></center>";

print "<p>Here is a list of GNOME Bugzilla products ";
if ($only_core_gnome) {
  print "in core GNOME ";
}
print "with the percentage of recent patches which have not been " .
      "reviewed for each.  Only patches submitted between " .
      "$min_days days and $max_days days ago are considered, and " .
      "only products which have at least $cutoff patches submitted " .
      "in that time are shown.</p>";

print "<p> NOTE: A patch is considered to have been reviewed if it " .
      "(a) has any status set";
if (!$count_obsolete) {
  print " (except obsolete; obsolete patches are ignored in this report)";
}
print ", or (b) is in a bug marked closed or resolved";
if ($use_blocked) {
  print ", or (c) is in a bug with the BLOCKED_BY_FREEZE keyword set";
}
print ".</p>";

print "<p><b>Important:</b> The fractions in the table are counts of " .
      "unreviewed patches versus total patches.  The number of " .
      "reviewed patches for each module is a straightforward " .
      "exercise left for the reader.  :-)</p>";

  my $query = "
    SELECT
      bugs.bug_id, bugs.product, attachments.attach_id,
        ( attachstatuses.statusid IS NULL  and";
  if ($count_obsolete) {
    $query .= "
          attachments.isobsolete!='1'      and";
  }
  $query .= "
          (
           bugs.bug_status='UNCONFIRMED' or
           bugs.bug_status='NEW' or
           bugs.bug_status='ASSIGNED' or
           bugs.bug_status='REOPENED'
          )";
  if ($use_blocked) {
    $query .= "
      and
      bugs.keywords NOT LIKE '%BLOCKED_BY_FREEZE%'";
  }
  $query .= "
        ) AS is_unreviewed
    FROM
      attachments
    LEFT JOIN
      attachstatuses ON attachments.attach_id=attachstatuses.attach_id
    LEFT JOIN
      bugs ON attachments.bug_id=bugs.bug_id";
  if ($only_core_gnome) {
    $query .= "
      LEFT JOIN
        products ON bugs.product = products.product";
  }
  $query .= "  
    WHERE
      attachments.ispatch='1'          and";
  if (!$count_obsolete) {
    $query .= "
          attachments.isobsolete!='1'      and";
  }
  $query .= "
      TO_DAYS(NOW()) - TO_DAYS(attachments.creation_ts) <= $max_days and
      TO_DAYS(NOW()) - TO_DAYS(attachments.creation_ts) >= $min_days";
  if ($only_core_gnome) {
    $query .= "
      and
      products.isgnome = 1";
  }
  $query .= "
    ORDER BY
      bugs.product, attachments.bug_id";

  print "\n\n";
  SendSQL ($query);

  my (@the_data_yo,
      $unreviewed_list, $patch_list,
      $unreviewed_count, $patch_count);
  my ($cur_product, $total_unreviewed_count, $total_count) = ('', 0, 0);

  while (my ($bug_id, $product, $attach_id, $is_unreviewed) = FetchSQLData()) {

    # Check if we've moved on to a new product and need to reset loop
    # counter-like variables
    if ($cur_product ne $product) {

      # If this isn't the very first product, we probably need to save
      # information for the last product we were gathering information
      # for.
      if ($cur_product ne '') {

        if ($patch_count < $cutoff) {
          $total_unreviewed_count -= $unreviewed_count;
          $total_count -= $patch_count;          
        } else {
          my $percentage = $unreviewed_count/$patch_count;
          push @the_data_yo, [$cur_product, $percentage, 
                              $unreviewed_count, $patch_count, 
                              $unreviewed_list, $patch_list];
        }
      }

      # Reset the data for the new product
      $cur_product   = $product;
      $patch_count = 0;
      $unreviewed_count = 0;
      $patch_list      = "http://bugzilla.gnome.org/buglist.cgi?bug_id=";
      $unreviewed_list = "http://bugzilla.gnome.org/buglist.cgi?bug_id=";
    }
    if ($is_unreviewed) {
      $unreviewed_list .= "$bug_id,";
      $unreviewed_count++;
      $total_unreviewed_count++;
    }
    $patch_list .= "$bug_id,";
    $patch_count++;
    $total_count++;
  }

  # Get the last product too
  if ($patch_count < $cutoff) {
    $total_unreviewed_count -= $unreviewed_count;
    $total_count -= $patch_count;          
  } else {
    my $percentage = $unreviewed_count/$patch_count;
    push @the_data_yo, [$cur_product, $percentage, 
                        $unreviewed_count, $patch_count, 
                        $unreviewed_list, $patch_list];
  }

  # Now, print in sorted order
  print "<table border=1 cellspacing=0 cellpadding=5>\n";
  print "<tr>
         <th>Product</th>
         <th>Unreviewed Patch %</th>
         </tr>\n";
  foreach my $listref (sort {$a->[1] <=> $b->[1]} @the_data_yo) {
    print "<tr> <td>$listref->[0]</td>";
    printf "<td> %3.0f", 100 * $listref->[1];
    print "% (";
    if ($listref->[2] > 0) {
      print "<a href=\"http://bugzilla.gnome.org/reports/patch-report.cgi?" .
            "product=$listref->[0]&min_days=$min_days&max_days=$max_days\">" .
            "$listref->[2]</a>";
    } else {
      print "$listref->[2]";
    }
    print "/";
    print "<a href=\"$listref->[5]\">$listref->[3]</a>";
    print ")</td></tr>\n";
  }
  my $percentage = $total_unreviewed_count/$total_count;
  printf "<tr><td>OVERALL</td> <td> %3.0f", 100 * $percentage;
  print "% ($total_unreviewed_count/$total_count)</td></tr>\n";
  print "</table><p>\n";

print "<!--" .
  "<p> SOME USEFUL PARAMETERS TO PASS THIS SCRIPT: <ul>\n" .
  "<li>min_days (Don't show patches younger than this)</li>\n" .
  "<li>max_days (Don't show patches older than this)</li>\n" .
  "<li>cutoff (Don't list products with fewer patches than this)</li>\n" .
  "<li>use_blocked (boolean, does BLOCKED_BY_FREEZE count as review)</li>\n" .
  "<li>count_obsolete (boolean, include obsolete patches in counts?)</li>\n" .
  "<li>only_core_gnome (boolean, only show core gnome products)</li>\n" .
  "</ul></p>\n" .
  "-->\n";

print "<p> If you spot any errors in this page please report it to the bugzilla component of bugzilla.gnome.org. Thanks.</p>";

PutFooter();
