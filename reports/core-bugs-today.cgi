#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.

# This is a cgi to reports bugs filed against 'core' desktop modules
# in the previous N days.

# NOTES: 
# * this was last updated for the 2.5 module list on 12/31/2003
# * pass 'days=X' to see X days before today.
# * pass 'includeowen=1' to see glib/gtk/pango, since owen has asked
#   volunteers not to touch those.
# * pass 'includelibs=1' to see libs, since libs are difficult for
#   most volunteers to triage
# * pass 'ignoretriaged=1' to ignore bugs with the bugsquad keyword.
# * pass 'keywords=FOO' to show only bugs with FOO as a keyword.

use diagnostics;
use strict;

eval "use GD";
my $use_gd = $@ ? 0 : 1;
$use_gd = 0 if $@;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product); # globals from er, globals.pl

ConnectToDatabase(1);

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

#save ourselves some writing later
my $days = $::FORM{'days'};
$days||=1; #default to 'today'
detaint_natural($days) || die "days parameter must be a number";

#build the query
#structure of the query is such:
#first list all acceptable states
#then dates [FIXME]
#then list of all top-level modules- if the module is a top-level 
#  bugzilla product, add things here
#then list of all lower-level modules- for example, we list applets
#  separately here because we wish to ignore many things in the 
#  gnome-applets product in bugzilla.


my $query = <<FIN;
select 
    distinct(bugs.bug_id)
from   bugs
where 
    bugs.creation_ts >= DATE_SUB(now(), INTERVAL $days DAY)
and
    bugs.creation_ts <= now()
and      
    (
     bugs.bug_status = 'UNCONFIRMED' or
     bugs.bug_status = 'NEW' or 
     bugs.bug_status = 'ASSIGNED' or 
     bugs.bug_status = 'REOPENED'
     )
and 
    (
     (
      bugs.product = 'acme' or
      bugs.product = 'audiofile' or
      bugs.product = 'bug-buddy' or
      bugs.product = 'control-center' or
      bugs.product = 'EOG' or
      bugs.product = 'epiphany' or
      bugs.product = 'esound' or
      bugs.product = 'gnome-mime-data' or
      bugs.product = 'gnome-vfs' or
      bugs.product = 'gcalctool' or
      bugs.product = 'GConf' or
      bugs.product = 'gdm' or
      bugs.product = 'gedit' or
      bugs.product = 'ggv' or
      bugs.product = 'gnome-desktop' or
      bugs.product = 'gnome-games' or
      bugs.product = 'gnome-icon-theme' or
      bugs.product = 'gnome-mag' or
      bugs.product = 'gnome-media' or
      bugs.product = 'gnome-panel' or
      bugs.product = 'gnome-session' or
      bugs.product = 'gnome-speech' or
      bugs.product = 'gnome-terminal' or
      bugs.product = 'gucharmap' or
      bugs.product = 'system-monitor' or
      bugs.product = 'gnome-themes' or
      bugs.product = 'gnome-user-docs' or
      bugs.product = 'gnomemeeting' or
      bugs.product = 'gnopernicus' or
      bugs.product = 'gok' or
      bugs.product = 'gpdf' or
      bugs.product = 'GStreamer' or
      bugs.product = 'gtksourceview' or
      bugs.product = 'libgtop' or
      bugs.product = 'metacity' or
      bugs.product = 'nautilus' or
      bugs.product = 'nautilus-cd-burner' or
      bugs.product = 'vte' or
      bugs.product = 'yelp' or
      bugs.product = 'zenity'
      )
     or
     (
      bugs.component = 'gnome-about' or
      bugs.component = 'cdplayer' or
      bugs.component = 'charpick' or
      bugs.component = 'deskguide' or
      bugs.component = 'gtik' or
      bugs.component = 'gweather' or
      bugs.component = 'mailcheck' or
      bugs.component = 'mini-commander' or
      bugs.component = 'mixer' or
      bugs.component = 'modemlights' or
      bugs.component = 'odometer' or
      bugs.component = 'screenshooter' or
      bugs.component = 'tasklist' or
      bugs.component = 'gcolorsel' or
      bugs.component = 'gfontsel' or
      bugs.component = 'gsearchtool' or
      bugs.component = 'logview' or
      bugs.component = 'meat-grinder' or
      bugs.component = 'stripchart' or
      bugs.component = 'gfloppy'
      )
FIN

# re-add gtk/pango/etc. if necessary for some reason
if($::FORM{'includeowen'}){
    $query.=<<FIN;
    or
        (
         bugs.product = 'gtk+' or
         bugs.product = 'gtk-docs' or
         bugs.product = 'pango' or
         bugs.product = 'glib'
         )
FIN
}

if($::FORM{'includelibs'}){
    $query.=<<FIN;
    or
        (
         bugs.product = 'at-spi' or
         bugs.product = 'atk' or
         bugs.product = 'bonobo' or
         bugs.product = 'bonobo-activation [was: oaf]' or
         bugs.product = 'eel' or
         bugs.product = 'ORBit2' or
         bugs.product = 'intltool' or
         bugs.product = 'libIDL' or
         bugs.product = 'libart' or
         bugs.product = 'libglade' or
         bugs.product = 'libgnome' or
         bugs.product = 'libgnomeprint' or
         bugs.product = 'libxml' or
         bugs.product = 'libxml2' or
         bugs.product = 'libxslt' or
         bugs.product = 'librsvg' or
         bugs.product = 'libwnck' or
         bugs.product = 'libzvt' or
         bugs.product = 'linc'
         )
FIN
}
    $query.=')';

# ignore triaged if the value it passed

    if($::FORM{'ignoretriaged'}){
        $query.='and bugs.keywords NOT LIKE \'%bugsquad%\'';
}

# add other arbitrary keywords- mostly useful for things like GNOMEVER

     if($::FORM{'keywords'}){
         $query.='and bugs.keywords LIKE ' . SqlQuote($::FORM{'keywords'});
     }

# End build up $query string
    SendSQL ($query);

my $buglist;
    while (my ($bug_id) = FetchSQLData()) {
        $buglist .= $bug_id;
        $buglist .= ',';
    }
print<<FIN;
<HEAD>
<meta http-equiv="refresh"content="1; url=http://bugzilla.gnome.org/buglist.cgi?bug_id=$buglist">
</HEAD>
FIN
exit;




