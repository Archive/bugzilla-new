#!/usr/bonsaitools/bin/perl -w
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# needinfo-updated.cgi
#
# Tell us which NEEDINFO reports have been given updated info.
#
# This file was based on the original mostfrequent.cgi but has been
# modified GREATLY by Wayne Schuller (k_wayne@linuxpower.org), Jan 2002.
#
# TODO
#	- Show stale NEEDINFO list as well? (eg: for mass closure)
#


use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product); # globals from er, globals.pl

# Output appropriate HTTP response headers
print "Content-type: text/html\n";
# Changing attachment to inline to resolve 46897 - zach@zachlipton.com
print "Content-disposition: inline; filename=bugzilla_report.html\n\n";

ConnectToDatabase(1);
# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
quietly_check_login();

PutHeader("NEEDINFO Reports Requiring Attention");

# Always allow them to see this menu.
report_menu();

# Always spew informative messages.
print "<p>When a bug report is filed that is lacking in info, the Gnome bughunters or the module maintainers will mark the bug as NEEDINFO.";
print "<p>This page gives a list of bugs which have been updated since the NEEDINFO status was sent. This is hopefully because the original reported has replied with the requested information. (Often it will just be bughunters making comments or marking duplicates - we haven't figured out how to not include these yet)</p>";

# If no parameters are passed we list all the reports for all products.
if (! defined $::FORM{'product'}){
	print_needinfo_updates();
} else {
	my $product = $::FORM{'product'};
	print_needinfo_updates($product);
}

print "<p> If you spot any errors in this page please report it to <a href=\"mailto:bugmaster\@gnome.org\">bugmaster\@gnome.org</a>. Thanks.</p>";

PutFooter();


# report_menu
# Display a drop down menu allowing the user to call this script on other
# modules/product.
sub report_menu {

	GetVersionTable(); # Need this for legal_product to be created. 

	# Does this work? Or do we need the second argument?
	my $product_popup = make_options(\@::legal_product);

print <<FIN;
<h4>Show this list for the following product:</h4>
<form method=get action=needinfo-updated.cgi>
<table border=1 cellpadding=5>
<tr>
<td align=center><b>Product:</b></td>
<td align=center>
<select name="product">
$product_popup
</select>
</td>
</tr>
<tr>
<td colspan=2 align=center>
<input type=submit value='Display Report'>
</td>
</tr>
</table>
</form>
<p>
FIN
}


# print_needinfo_updates(product);
#
# Parameters:
#	product = (string) bugzilla product name
#
#
#
sub print_needinfo_updates {
	my($product) = @_;

	my $query;
	my $query_resolved;

	# Process parameters
	if (!$product) {
		$product = "%"; # Matches any product.
	}

	# We are going to build a long SQL query.
	# Attempt to see find a bugs_activity entry where NEEDINFO was marked
	# and that the current bug modification is newer than this date.
	# We give an extra 15 minutes grace, because the bug hunter
	# may make some comments in that time, and we don't want to get those.
	# FIXME: This query could be improved somewhat. It lists many
	# examples of comments by bughunters or duplicates which we don't
	# want to see.
	my $quoted_product = SqlQuote($product);
	$query = <<FIN;
select
	bugs.bug_id, assign.login_name, bugs.short_desc, bugs.product, bugs.component
from 
	bugs, bugs_activity, profiles assign
where 
	bugs.bug_status='NEEDINFO'
and
	bugs.product LIKE $quoted_product
and 
	bugs.bug_id = bugs_activity.bug_id
and 
	bugs_activity.added='NEEDINFO'
and 
	(bugs_activity.bug_when + INTERVAL 15 MINUTE) < bugs.lastdiffed
and
	bugs.assigned_to = assign.userid

group by bug_id 
FIN

	# Print a nice cross-referenced table of results.
	print "<table border=1 cellspacing=0 cellpadding=5>\n";
	print "<tr><th>Bug ID</th><th>Owner</th><th>Product</th><th>Summary</th></tr>\n";
	SendSQL ($query);
	while (my ($bug_id, $assigned_to, $summary, $product, $component) = FetchSQLData()) {

print <<FIN;
        <tr>
	<td><a href="../show_bug.cgi?id=$bug_id">$bug_id</a></td>
	<td><a href="mailto:$assigned_to">$assigned_to</a></td>
	<td><a href="http://bugzilla.gnome.org/buglist.cgi?product=$product&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">$product</a>/<a href="http://bugzilla.gnome.org/buglist.cgi?product=$product&component=$component&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED">$component</a></td>
	<td>$summary</td>
	</tr>
FIN
	}
	print "</table><p>\n";
}
