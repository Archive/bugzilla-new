#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.

#the goal of this page is to give Luis or other people with sudo access as the
#bugzilla user a script that they can quickly modify to give them arbitrary SQL
#without having to use CVS or a front end that can allow arbitrary SQL.

use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "globals.pl";
require "CGI.pl";
use vars qw(@legal_product); # globals from er, globals.pl

ConnectToDatabase(1);

my $num = 10;
if (defined $::FORM{'num'}) {
  $num = 10*$::FORM{'num'};
}

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

#add arbitrary query here
my $query = <<FIN;
select
    distinct(bugs.bug_id)
from   bugs
where
(
 (	
  bugs.bug_status='UNCONFIRMED' or
  bugs.bug_status='NEW' or
  bugs.bug_status='ASSIGNED' or
  bugs.bug_status='REOPENED'
 )
and
 bugs.bug_severity!='enhancement'
and
 bugs.delta_ts < '2004-04-01'
and
 bugs.product != 'gtk+'
and
 bugs.product != 'gimp'
)
ORDER BY bug_id
LIMIT $num, 10
FIN

# End build up $query string
    SendSQL ($query);

my $buglist;
    while (my ($bug_id) = FetchSQLData()) {
        $buglist .= $bug_id;
        $buglist .= ',';
    }
print<<FIN;
<HEAD>
<meta http-equiv="refresh" content="0; url=http://bugzilla.gnome.org/buglist.cgi?bug_id=$buglist">
</HEAD>
FIN
exit;

