#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-

use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory

use lib ".";
require "CGI.pl";
use vars qw(
    %FORM
    $template
    %components
    @legal_product
    @legal_components
    %components
    $vars
); # globals from CGI.pl

require "globals.pl";

#set up the page
ConnectToDatabase(1);
GetVersionTable();

my $product = $::FORM{'product'};
my $component = $::FORM{'component'};

if(!defined($product) || $product eq '')
{
    my @products = @::legal_product;
    @products = sort { lc($a) cmp lc($b) } @products;
    $vars->{'product'} = \@products;
    

    # Sort the component list...
    my $comps = \%::components;
    foreach my $p (@products) {
        my @tmp = sort { lc($a) cmp lc($b) } @{$comps->{$p}};
        $comps->{$p} = \@tmp;
    }
    
    $vars->{'componentsbyproduct'} = $comps;
    #$vars->{'component_'} = \@components;
    $vars->{'component_'} = \@::legal_components;

    quietly_check_login();

    print "Content-type: text/html\n\n";    
    $template->process("patch-status.html.tmpl", $vars)
      || ThrowTemplateError($template->error());
} else {
  print "Content-type: text/html\n\n";
  CheckFormField(\%::FORM, 'product',      \@::legal_product);
  print "<HEAD>\n" .
        "<meta http-equiv=\"refresh\" content=\"0; " .
        "url=http://bugzilla.gnome.org/reports/patch-report.cgi?" .
        "product=$product&component=$component\">\n" .
        "</HEAD>\n";
}

#PutFooter();
