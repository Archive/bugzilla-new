#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
require "globals.pl";

print "Content-type: text/html\n\n";

# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
quietly_check_login();

PutHeader ("Reports");

print <<FIN;

<h3>Categories</h3>
  <ul>
    <li> <a href="#generic">Generic Gnome reports</a> </li>
    <li> <a href="#maintainers">Reports for maintainers</a> </li>
    <li> <a href="#byversion">Reports organized by version</a> </li>
    <li> <a href="#explain">Explanation of bugzilla terms and fields</a></li>
  </ul>

<h2>Generic Gnome reports<a name="generic"></a></h2>
  <ul>
    <li>
      <a href="core-bugs-today.cgi">Bugs filed today</a> against
      components listed at <a href=
      "http://www.gnome.org/start/unstable/modules/">
      gnome.org/start/</a> as core GNOME components.  </li>
    <li>
      <a href="weekly-bug-summary.html">Summary of bug activity for
      the last week</a>.
    </li>
  <!--
    <li>
      <a href="product-keyword-report.cgi">Open bugs listed by product
      and keyword</a>.
    </li>
    <li>
      <a href="product-target-report.cgi">Open bugs listed by product
      and target</a>.
    </li>
  -->
    <li>
      <a href="unconfirmed-bugs.cgi">Products with the most
      UNCONFIRMED bugs</a>. Good starting point for new bughunters
      looking to triage bugs.
    </li>
    <li>
      <a href="recent-mostfrequent.cgi">Frequently (and recently)
      duplicated bugs</a>
    </li>
    <li>
      <a href="needinfo-updated.cgi">NEEDINFO reports which have been
      updated</a> (e.g. given new info).
    </li>
    <li>
      <a href="check-assignedto.cgi">Bugs likely not correctly assigned to the
      correct owner</a> Shows bugs owned by the default owner of another
      component.
    </li>
  </ul>

<h2>Reports for Maintainers<a name="maintainers"></a></h2>
  <ul>
    <li>Unreviewed patches:
      <ul>
        <li>
          <a href="patch-report.cgi"> Comprehensive listing</a> (or by
          <a href="patch-status.cgi"> product</a>).
        </li>
        <li>
          Products with the <a href="patch-diligence-report.cgi">
          fewest unreviewed patches per total patches</a>.
        </li>
      </ul>
    </li>
    <li>Generic, editable queries for product foo:
      <ul>
        <li>
          <a href="boogle.cgi?query=product:foo%20patch-status:none">
          Bugs with unreviewed patches</a>.
        </li>
        <li>
          <a href=
          "boogle.cgi?query=product:foo%20patch-status:accepted-commit_after_freeze">
          Bugs with patches marked accepted-commit_after_freeze</a>.
        </li>
        <li>
          <a href=
          "boogle.cgi?query=product:foo%20patch-status:accepted-commit_now">
          Bugs with patches marked accepted-commit_now</a>.
        </li>
        <li>
          <a href=
          "boogle.cgi?query=product:foo%20status:needinfo">
          All needinfo bugs</a>.
        </li>
        <li>
          <a href=
          "boogle.cgi?query=product:foo%20comment-count:1">
          Bugs which have not been commented on</a>.
        </li>
        <li>
          <a href=
          "boogle.cgi?query=product:foo%20crash%20start%20meta-status:all">
          Bugs which have the words "crash" and "start", including
          bugs that aren't open</a>.
        </li>
      </ul>
    </li>
    <li>
        <a href="gnome-love.cgi">Bugs suitable as tasks for new
        developers</a> (okay, so this report is more for contributors,
        but it is here to remind you to add the gnome-love keyword to
        appropriate bugs)
    </li>
  </ul>

<h2>Reports organized by version<a name="byversion"></a></h2>
  <ul>
    <li> Open bugs for specific GNOME versions, broken down by
         product, component, and developer:
      <ul>
        <!--
          --These are no longer generated--
        <li><a href="gnome-20-report.html">GNOME 2.0</a> </li>
        <li><a href="gnome-22-report.html">GNOME 2.1/2.2</a> </li>
        <li><a href="gnome-24-report.html">GNOME 2.3/2.4</a> </li>
        <li><a href="gnome-26-report.html">GNOME 2.5/2.6</a> </li>
        -->
        <li><a href="gnome-28-report.html">GNOME 2.7/2.8</a> </li>
        <li><a href="gnome-210-report.html">GNOME 2.9/2.10</a> </li>
        <li><a href="gnome-212-report.html">GNOME 2.11/2.12</a> </li>
        <li><a href="gnome-214-report.html">GNOME 2.13/2.14</a> </li>
      </ul>
    </li>
    <li> Summary of bug activity for the last week, by GNOME version:
      <ul>
        <!--
          --These are no longer generated--
        <li> <a href="weekly-bug-summary-gnome20.html">GNOME 2.0</a> </li>
        <li> <a href="weekly-bug-summary-gnome22.html">GNOME 2.1/2.2</a> </li>
        <li> <a href="weekly-bug-summary-gnome24.html">GNOME 2.3/2.4</a> </li>
        <li> <a href="weekly-bug-summary-gnome26.html">GNOME 2.5/2.6</a> </li>
        -->
        <li> <a href="weekly-bug-summary-gnome28.html">GNOME 2.7/2.8</a> </li>
        <li> <a href="weekly-bug-summary-gnome210.html">GNOME 2.9/2.10</a></li>
        <li> <a href="weekly-bug-summary-gnome212.html">GNOME 2.11/2.12</a></li>
        <li> <a href="weekly-bug-summary-gnome212.html">GNOME 2.13/2.14</a></li>
      </ul>
    </li>
  </ul>

<h2>Explanation of various bugzilla terms and fields<a name="explain"></a></h2>
  <ul>
    <li>
      <a href="../describecomponents.cgi">Products</a>
    </li>
    <li>
      <a href="../bug_status.html">Basic states</a>
      <ul>
        <li> 
          <a href="../bug_status.html#status">Bug status</a>
        </li>
        <li>
          <a href="../bug_status.html#resolution">Resolution</a>
        </li>
        <li>
          <a href="../bug_status.html#severity">Severities</a>
        </li>
        <li>
          <a href="../bug_status.html#priority">Priorities</a>
        </li>
        <li>
          <a href="../bug_status.html#gg_version">Gnome Version</a>
        </li>
        <li>
          <a href="../bug_status.html#gg_milestone">Gnome Target Milestone</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="../describekeywords.cgi">Keywords</a>
    </li>
    <li>
      <a href="../describeattachstatuses.cgi">Attachment statuses</a>
    </li>
  </ul>

If you have questions or suggestions for new reports, email <a href="mailto:bugmaster\@gnome.org">the GNOME bugmasters.</a><br><br>
FIN
PutFooter();

exit;

