#!/usr/bonsaitools/bin/perl -w
#
# Copyright 2004, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "recent-mostfrequent-utils.pl";

# Output appropriate HTTP response headers
print "Content-type: text/html\n";
# Changing attachment to inline to resolve 46897 - zach@zachlipton.com
print "Content-disposition: inline; filename=recent-mostfrequent.html\n\n";

ConnectToDatabase(1);
# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
quietly_check_login();

# make sensible defaults.
my $days = 30;         # Don't show duplicates older than this
my $cutoff = 3;        # Don't list bugs with fewer dupes than this
my $only_core_gnome=0; # (boolean) whether to only show core gnome products

if (defined $::FORM{'days'} && $::FORM{'days'} ne ""){
  $days = $::FORM{'days'};
}

if (defined $::FORM{'cutoff'} && $::FORM{'cutoff'} ne ""){
  $cutoff = $::FORM{'cutoff'};
}

if (defined $::FORM{'only_core_gnome'} && $::FORM{'only_core_gnome'} ne ""){
  $only_core_gnome = $::FORM{'only_core_gnome'};
}

PutHeader("Frequently reported bugs");

# Always spew informative messages.
print "\n";
print "<center><h1>Frequently (and recently) reported bugs</h1></center>\n";

print "<p>Here is a list of bugs ";
if ($only_core_gnome) {
  print "in core GNOME ";
}
print "that have had $cutoff or more duplicate reports filed in the last " .
      "$days days.</p>\n";

my $print_warnings = 1;

  my @new_data = 
    get_recent_mostfrequent_table($days, $only_core_gnome, $print_warnings);

  # Now, print in sorted order
  print "<table border=1 cellspacing=0 cellpadding=5>\n";
  print "<tr>
         <th>Bug#</th>
         <th>Duplicates</th>
         <th>Status</th>
         <th>Resolution</th>
         <th>Description</th>
         </tr>\n";
  foreach my $listref (reverse sort {$a->[1] <=> $b->[1]} @new_data) {
    if ($listref->[1] < $cutoff) {
      next;
    }
    my $query = "
      SELECT
        substring(bugs.bug_status,1,4),
        substring(bugs.resolution,1,4),
        substring(bugs.short_desc, 1, 60)
      FROM
        bugs
      WHERE
        bugs.bug_id = $listref->[0]
      ";
    SendSQL($query);
    my ($status, $resolution, $desc) = FetchSQLData();
    print "<tr>\n";
    print "<td><a href=\"http://bugzilla.gnome.org/show_bug.cgi?id=" .
              "$listref->[0]\">$listref->[0]</a></td>";
    print "<td><a href=\"$listref->[2]\">$listref->[1]</a></td>";
    print "<td>$status</td>\n";
    print "<td>$resolution</td>\n";
    print "<td>$desc</td>\n";
    print "</tr>\n";
  }
  print "</table>\n";

print "<!--" .
  "<p> SOME USEFUL PARAMETERS TO PASS THIS SCRIPT: <ul>\n" .
  "<li>days (Don't show duplicates marked more days ago than this)</li>\n" .
  "<li>cutoff (Don't show bugs with fewer duplicates than this)</li>\n" .
  "<li>only_core_gnome (boolean, only show core gnome products)</li>\n" .
  "</ul></p>\n" .
  "-->\n";

print "<p> If you spot any errors in this page please report it to the bugzilla component of bugzilla.gnome.org. Thanks.</p>";

PutFooter();
