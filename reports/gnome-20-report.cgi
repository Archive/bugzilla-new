#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Harrison Page <harrison@netscape.com>,
# Terry Weissman <terry@mozilla.org>,
# Dawn Endico <endico@mozilla.org>
# Bryce Nesbitt <bryce@nextbus.COM>,
#    Added -All- report, change "nobanner" to "banner" (it is strange to have a
#    list with 2 positive and 1 negative choice), default links on, add show
#    sql comment.
# Joe Robins <jmrobins@tgix.com>,
#    If using the usebuggroups parameter, users shouldn't be able to see
#    reports for products they don't have access to.
# Gervase Markham <gerv@gerv.net> and Adam Spiers <adam@spiers.net>
#    Added ability to chart any combination of resolutions/statuses.
#    Derive the choice of resolutions/statuses from the -All- data file
#    Removed hardcoded order of resolutions/statuses when reading from
#    daily stats file, so now works independently of collectstats.pl
#    version
#    Added image caching by date and datasets
# Myk Melez <myk@mozilla.org):
#    Implemented form field validation and reorganized code.
#
# Luis Villa <louie@ximian.com>:
# modified from evolution to gnome2 stuff
#
#    TODO:  sort component table by product then by bug totals
#           sorting ATM is by total # of bugs
#           needs to have a 'total' column at the bottom.

use diagnostics;
use strict;

eval "use GD";
my $use_gd = $@ ? 0 : 1;
eval "use Chart::Lines";
$use_gd = 0 if $@;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product); # globals from er, globals.pl

my @status = qw (NEW ASSIGNED REOPENED);
my @products;
my @severities = qw (Immediate Urgent High Normal Low);

my %bugs_severity;
my %bugs_per_item;

my %bugs_per_product ;
my %bugs_per_component;
my %bugs_per_developer;

my %resolutions_per_developer;
my %resolutions_per_product;
my %fixes_per_developer;

# while this looks odd/redundant, it allows us to name
# functions differently than the value passed in

# If we're using bug groups for products, we should apply those restrictions
# to viewing reports, as well.  Time to check the login in that case.
ConnectToDatabase(1);
quietly_check_login();

GetVersionTable();

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

PutHeader("GNOME 2.0 Bug Report",'');
evo_current_report();
print "<p>";

print("</body></html>");

PutFooter();

sub sort_by_total{
    $bugs_severity{$a}{'High'} <=> $bugs_severity{$b}{'High'}
}
sub sort_by_fixes{
    $fixes_per_developer{$a}{'showstopper'} <=> $fixes_per_developer{$b}{'showstopper'}
}

sub evo_current_report {

# Build up $query string
#FIXME: ponder including unconfirmed here
    my $query = <<FIN;
select 
    bugs.bug_id, bugs.priority,
    bugs.product, bugs.component 
from   bugs
where
    (
     bugs.keywords LIKE '%GNOME2%'
     )
and
    (
     bugs.bug_severity != 'enhancement'
     )
and      
    (
     bugs.bug_status = 'NEW' or 
     bugs.bug_status = 'ASSIGNED' or 
     bugs.bug_status = 'REOPENED'
     )
FIN
# End build up $query string
    SendSQL ($query);
    
    my $c = 0;

    my $bugs_reopened = 0;
    my $product;
    my $severity;
    my @products_list;
    push @products_list, 'Total';
    my %bugs_totals;
    my %bugs_lookup;

    #############################
    # suck contents of database # 
    #############################

    while (my ($bid, $pri, $prod, $comp) = FetchSQLData()) {
        next if (exists $bugs_lookup{$bid});

        if(!defined($bugs_per_item{$prod})) #god this is a hack
        {
            $bugs_severity{$prod}{'High'}=0;
        }

        $bugs_lookup{$bid} ++;

        $bugs_severity{$prod}{$pri} ++;
        $bugs_severity{'Total'}{$pri} ++;
        $bugs_severity{$prod}{'Total'} ++;

        $bugs_per_item{$prod} ++;
        $bugs_per_item{'Total'} ++;

        if($bugs_per_item{$prod}==5) #sort of hackish way of only showing really buggy products
        {
            push @products_list, $prod;
        }
    }

 
#Product table

print <<FIN;
    <center><h1 id="product">Bug Count by Product for GNOME2</h1></center>
        Like any bug tracking system, bugzilla.gnome.org (and therefore the counts below) are imperfect- these numbers include a number of duplicates and overlaps, especially after the surge of a major release. If you want to help clean up bugzilla, and make it more useful for the GNOME developers, you can read <a href="triage.cgi">our triage guide</a> or come by irc.gnome.org #bugs.<p>
        This page is re-generated every 1/2 hour to reduce load on the server.<p>
<center>
    <table border=3 cellpadding=5>
    <tr>
    <td align=center bgcolor="#DDDDDD"><b>Product</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Immediate</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Urgent</b></td>
    <td align=center bgcolor="#DDDDDD"><b>High</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Normal</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Low</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Total</b></td>
    </tr>
FIN

#here we hopefully loop out the table
    @products_list = reverse sort sort_by_total @products_list;

    foreach $product (@products_list) {
        print('<tr><td align=left><tt>'.substr($product,0,16).'</tt></td>');
                foreach $severity (@severities) {
                    $bugs_severity{$product}{$severity}||='<font color="white">0</font>';
                    my $url_product = $product;
                    if($url_product eq 'Total'){$url_product = '';}
print <<FIN;
        <td align=center><a href="http://bugzilla.gnome.org/buglist.cgi?priority=$severity&product=$url_product&keywords=GNOME2&keywords_type=allwords&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=blocker&bug_severity=critical&bug_severity=major&bug_severity=normal&bug_severity=minor&bug_severity=trivial">$bugs_severity{$product}{$severity}</a></td>
FIN
}
print <<FIN;

        <td align=center><a href="http://bugzilla.gnome.org/buglist.cgi?product=$product&keywords=GNOME2&keywords_type=allwords&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=blocker&bug_severity=critical&bug_severity=major&bug_severity=normal&bug_severity=minor&bug_severity=trivial&priority=High&priority=Urgent&priority=Immediate">$bugs_per_item{$product}</a></td></tr>

FIN

}                   
print <<FIN;
</table>
</center>
<p>
FIN

}



