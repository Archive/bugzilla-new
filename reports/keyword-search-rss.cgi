#!/usr/bonsaitools/bin/perl -w
#
# Copyright 2005, Elijah Newren
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "keyword-search-utils.pl";
use vars qw(@legal_product); # globals from er, globals.pl

use POSIX qw(ceil floor);
use XML::RSS;

ConnectToDatabase(1);
# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.

# make sensible defaults.
my $only_core_gnome=0;    # (boolean) whether to only show core gnome products
my $show_setter=1;        # (boolean) whether to show who set the keyword
my $product="%";          # (string) which product to search, % for all
my $keyword="gnome-love"; # (string) which keyword to search

if (defined $::FORM{'only_core_gnome'} && $::FORM{'only_core_gnome'} ne ""){
  $only_core_gnome = $::FORM{'only_core_gnome'};
}
if (defined $::FORM{'show_setter'} && $::FORM{'show_setter'} ne ""){
  $show_setter = $::FORM{'show_setter'};
}
if (defined $::FORM{'product'} && $::FORM{'product'} ne ""){
  $product = $::FORM{'product'};
  $only_core_gnome = 0;  # The product might be outside core gnome...
}
if (defined $::FORM{'keyword'} && $::FORM{'keyword'} ne ""){
  $keyword = $::FORM{'keyword'};
}

# Output appropriate HTTP response headers
print "Content-type: text/xml\n";
# Changing attachment to inline to resolve 46897 - zach@zachlipton.com
print "Content-disposition: inline; " .
      "filename=keyword-search-" . $keyword . ".rss\n\n";

my @data = get_keyword_bug_info($only_core_gnome, $product, $keyword);

my $query = "
  SELECT
    description
  FROM
    keyworddefs
  WHERE
    name = '$keyword'
  ";
SendSQL ($query);
my ($keyword_desc) = FetchSQLData();

my $rss = new XML::RSS (version => '1.0');
$rss->channel(
  title        => "Gnome " . $keyword . " bugs",
  link         => "http://bugzilla.gnome.org/",
  description  => "Bugs matching the following criteria: $keyword_desc",
);

sub intelligently {
 my $aval = $a->{patch_count}*10 + $a->{comment_count} + floor($a->{age}/365);
 my $bval = $b->{patch_count}*10 + $b->{comment_count} + floor($b->{age}/365);
 return $aval     <=> $bval     ||
        $a->{age} <=> $b->{age} ||
        $b->{id}  <=> $a->{id};
}

my $total = 0;
foreach my $bug (sort intelligently @data) {
  $rss->add_item(
    title       => $bug->{description},
    link        => "http://bugzilla.gnome.org/show_bug.cgi?id=" . $bug->{id},
    description => "<pre>" . 
                   quoteUrlsOutsideBug($bug->{id},
                                       substr($bug->{comment}, 0, 400)) .
                   "</pre>",
    dc => {
      subject  => $bug->{product},
      creator  => $bug->{who}
    }
  );
}

print $rss->as_string;

# vim: sw=2 smarttab expandtab:
