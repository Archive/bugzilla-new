#!/usr/bonsaitools/bin/perl -w
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# check-assignedto.cgi
#
# Tell us 
#
# This file was based on the original needinfo-updated.cgi but has been
# modified by Olav Vitters (olav@bkor.dhs.org), Jan 2005.
#

use diagnostics;
use strict;

# Bugzilla doesn't expect us to be running in a reports subdirectory, so we
# do a little magic to trick it.
use Cwd;
my $dir = cwd;
if ($dir =~ /reports$/) {
  chdir "..";
}
push @INC, "reports/."; # many scripts now are in the reports subdirectory
 
require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product); # globals from er, globals.pl

# Output appropriate HTTP response headers
print "Content-type: text/html\n";
# Changing attachment to inline to resolve 46897 - zach@zachlipton.com
print "Content-disposition: inline; filename=bugzilla_report.html\n\n";

ConnectToDatabase(1);
# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
quietly_check_login();

PutHeader("Bugs assigned to default owners of other products/components");

# Always spew informative messages.
print "<p>This reports shows bugs that:<ol><li>Are <i>not</i> assigned to the default owner of the product/component</li><li>Are assigned to someone being an default owner of another product/component</li></ol></p><p>The purpose is showing bugs that haven't correctly been moved from one product to another.</p>";

print_assignedto_differences();


print "<p> If you spot any errors in this page please report it to <a href=\"mailto:bugmaster\@gnome.org\">bugmaster\@gnome.org</a>. Thanks.</p>";

PutFooter();


# print_needinfo_updates(product);
#
# Parameters:
#	product = (string) bugzilla product name
#
#
#
sub print_assignedto_differences {
#	my($product) = @_;

	my $query;
	my $query_resolved;

#	# Process parameters
#	if (!$product) {
#		$product = "%"; # Matches any product.
#	}

	# We are going to build a long SQL query.
	# Attempt to see find a bugs_activity entry where NEEDINFO was marked
	# and that the current bug modification is newer than this date.
	# We give an extra 15 minutes grace, because the bug hunter
	# may make some comments in that time, and we don't want to get those.
	# FIXME: This query could be improved somewhat. It lists many
	# examples of comments by bughunters or duplicates which we don't
	# want to see.
#	my $quoted_product = SqlQuote($product);
	$query = <<FIN;
SELECT
	 components.initialowner
FROM 
	 products INNER JOIN components on products.product = components.program
         INNER JOIN profiles ON profiles.userid = components.initialowner
WHERE
	 components.program = products.product
  AND 	 products.disallownew = '0'
  AND    profiles.login_name LIKE '%\@gnome.bugs'
GROUP BY components.initialowner
FIN

	# Print a nice cross-referenced table of results.
	print "<table border=1 cellspacing=0 cellpadding=5>\n";
	print "<tr><th>Bug ID</th><th>Current Owner</th><th>Default Owner</th><th>Product</th><th>Summary</th></tr>\n";
	SendSQL ($query);
	my @defaultowners;
	my $defaultowners;
	push(@defaultowners, FetchOneColumn()) while MoreSQLData();
	$defaultowners = join(",", @defaultowners);
	$query = <<FIN;
SELECT   bugs.bug_id, bugs.short_desc, bugs.product, bugs.component, a.login_name, b.login_name
FROM     bugs INNER JOIN components ON bugs.product = components.program AND bugs.component = components.value
         INNER JOIN profiles a ON bugs.assigned_to = a.userid
         INNER JOIN profiles b ON components.initialowner = b.userid
WHERE    bugs.assigned_to <> components.initialowner
  AND    bugs.bug_status IN ('UNCONFIRMED', 'NEW')
  AND    bugs.assigned_to IN ($defaultowners)
  AND    a.login_name LIKE '%\@gnome.bugs'
ORDER BY bugs.product ASC
FIN
	SendSQL ($query);
#
	my $e_assigned;
	my $e_initial;
	my @buglist;
	while (my ($bug_id, $summary, $product, $component, $assigned_to, $initialowner) = FetchSQLData()) {
		$assigned_to =~ y/\@/ /;
		$initialowner =~ y/\@/ /;;
		$summary = html_quote($summary);
print <<FIN;
        <tr>
	<td><a href="../show_bug.cgi?id=$bug_id">$bug_id</a></td>
	<td>$assigned_to</td>
	<td>$initialowner</td>
	<td>$product: $component</td>
	<td>$summary</td>
	</tr>
FIN
	push (@buglist, $bug_id);
	}
	print "</table><p>\n";
	print "<p><a href=\"/buglist.cgi?bug_id=" . join(",", @buglist) . "\">buglist</a></p>";
}
