#!/usr/bonsaitools/bin/perl -w

# (c) Copyright Elijah Newren, Olav Vitters 2005
# Licensed under whatever Free/Open Source license is necessary to
# allow this to be in upstream bugzilla (if they want my ugly hacks)
# and be the most useful to the Gnome Bugsquad (is there a "please
# bury this code deep beneath the Ocean's bed and pretend one of our
# people never wrote it" license?).  I give permission to the Gnome
# Foundation board of directors to declare what that means.
#
# Sucks to be you to have to work to figure out the license, doesn't
# it?  Well, better you than me.  ;-)

  use QueryParse;

  # Bugzilla doesn't expect us to be running in a reports subdirectory, so we
  # do a little magic to trick it.
  use Cwd;
  my $dir = cwd;
  if ($dir =~ /reports$/) {
    chdir "..";
  }
  push @INC, "reports/."; # many scripts now are in the reports subdirectory
   
  require "CGI.pl";
  use vars qw(%FORM); # globals from CGI.pl

  require "globals.pl";

  my $query = $::FORM{'query'};
  my $showhelp = $::FORM{'showhelp'};
  my $button_query = $::FORM{'btnQ'};
  my $button_help = $::FORM{'btnH'};
  
  # Show/hide help
  $showhelp = $showhelp ? 1 : 0;
  if ($button_help) {
    $showhelp = 1 - $showhelp;
  }

#$query = 'gnome-version:single';
#$button_query = 1;
#print "Warning: modified the query for debugging purposes\n";

  # Output appropriate HTTP response headers
  print "Content-type: text/html\n";
  # Changing attachment to inline to resolve 46897 - zach@zachlipton.com
  print "Content-disposition: inline; filename=boogle.cgi\n\n";

  ConnectToDatabase(1);
  # When we show the footer, it's confusing to claim the user isn't logged in
  # if they are.
  quietly_check_login();

  PutHeader("Boogle");
  print "
    <center>
    <h1>Boogle</h1><font color=red>*Experimental!*</font>
    </center>
    ";

sub show_help
{
  print "
    <center>
    Example searches:<br/>
    <p>nautilus click crash</p>
    <p>\"Wooo! 100,000 bugs!\" status:resolved</p>
    <p>product:gtk+ patch-status:needs-work</p>
    <p>component:general keyword:memory summary:leak</p>
    <p>(summary:focus OR priority:high,urgent) AND product:metacity,libwnck</p>
    </center>

    <p>meta-status:open is added to all queries where status and
    meta-status are not specified.</p>

    <p>If you enter an invalid search field
    (e.g. 'badfield:blablabla') or search term
    (e.g. 'priority:doitrightnow') and have no syntax errors, you
    should be given a list or link to valid terms.  If you enter
    multiple invalid entries, you will only be given help for the
    first such error.</p>
    ";
}

sub show_input_box
{
  my ($query, $error, $showhelp) = @_;

  $query = '' unless defined $query;
  my $button_text = "Perform the search";

  my $quoted_query = html_quote($query);
  my $showhelpstring = $showhelp ? "Hide help" : "Show help";
  my $showhelptoggle = $showhelp;
  
  print "
    <center>
    <form action=\"boogle.cgi\" method=\"POST\">
      <input maxLength=256 size=55 type=\"text\" name=\"query\"
             value=\"$quoted_query\"><br/>";
  if ($error) {
    print "<font color=red>$error</font><br>\n";
    $button_text = "Try again, I've fixed the error--I promise";
  }
  print "
      <input type=\"submit\" name=\"btnQ\" value=\"$button_text\">
      <input type=\"hidden\" name=\"showhelp\" value=\"$showhelptoggle\">
      <input type=\"submit\" name=\"btnH\" value=\"$showhelpstring\">
    </form>";

  print "</center>\n";

  # Some extra space would be nice...
  print "<p></p>\n";

  if ($showhelp) {
    show_help();
  }
}

sub handle_query
{
  my ($query) = @_;

  if (UserInGroup('maintainers')) {
    print "<pre>\n";
    print $query;
    print "</pre>\n";
  }

  print "<p>Searching...";
  my $bug_list = "http://bugzilla.gnome.org/buglist.cgi?bug_id=";
  my $bug_count = 0;
  SendSQL ($query);
  while (my ($bug_id) = FetchSQLData()) {
    $bug_list .= $bug_id . ",";
    $bug_count++;
    if ($bug_count >= 1000) {
      print "<font color=red>Too many bugs!</font>  " .
            "Only first 1000 provided.</p>\n<p>";
      last;
    }
  }

  # Remove the trailing comma
  chop($bug_list);

  if ($bug_count > 0) {
    print "<a href= \"$bug_list\">Enjoy your bugs</a>!</p>\n";
  } else {
    print "<font color=red>no bugs matching your criteria were found!</font>" .
          "</p>\n";
  }
}


  if(!$button_query || !$query) {
    show_input_box($query, '', $showhelp);
  } else {
    my ($parser) = new QueryParse;
    my $result = $parser->parse($query);
    if (defined $result->{error}) {
      show_input_box($query, $result->{error}, $showhelp);
    } else {
      handle_query($result->{query});
    }
  }

  PutFooter();
