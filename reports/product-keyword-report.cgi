#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Mozilla Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bugzilla Bug Tracking System.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): Harrison Page <harrison@netscape.com>,
# Terry Weissman <terry@mozilla.org>,
# Dawn Endico <endico@mozilla.org>
# Bryce Nesbitt <bryce@nextbus.COM>,
#    Added -All- report, change "nobanner" to "banner" (it is strange to have a
#    list with 2 positive and 1 negative choice), default links on, add show
#    sql comment.
# Joe Robins <jmrobins@tgix.com>,
#    If using the usebuggroups parameter, users shouldn't be able to see
#    reports for products they don't have access to.
# Gervase Markham <gerv@gerv.net> and Adam Spiers <adam@spiers.net>
#    Added ability to chart any combination of resolutions/statuses.
#    Derive the choice of resolutions/statuses from the -All- data file
#    Removed hardcoded order of resolutions/statuses when reading from
#    daily stats file, so now works independently of collectstats.pl
#    version
#    Added image caching by date and datasets
# Myk Melez <myk@mozilla.org):
#    Implemented form field validation and reorganized code.
#
# Luis Villa <louie@ximian.com>:
#    modified it to report things in a new format

#README
#to edit/add report stuff to the 'new and wonderful' b.g.o. version:
#1) Does the intended product span multiple bugzilla products (i.e.,
#   'nautilus' consists of 'nautilus' and also medusa, and 'libs' might
#   consist (for example) of libgnomeui, libwnck, etc. If so, scroll down
#   to the Evolution section below for an example.
#2) If it's a single-product product, no fanciness is necessary to
#   construct a product; the page is smart enough to take individual
#   products.
#
#Once you've created the product group as in #1, or just read #2, edit 
#   @myproducts and/or @mymilestones to add additional values to the
#   popup menu. Note that values not in the popup menu but covered by
#   (2) will work just fine.


use diagnostics;
use strict;

eval "use GD";
my $use_gd = $@ ? 0 : 1;
eval "use Chart::Lines";
$use_gd = 0 if $@;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
use vars qw(@legal_product); # globals from er, globals.pl

my @priorities = qw (Urgent High Normal Low);
my @severities = qw (blocker critical major normal minor trivial enhancement);

#here we define the various products.
#basically, $product{'product_name'}{'SQL_string'} is bugs.product=whatever or bugs.product=whatever. This is for the SQL query.
# $product{'product_name'}{'search_string'} is product=whatever&product=whatever2. This is for the bugzilla links.
# $product{'product_name'}{'pretty_name'} is a pretty name for the title.

my %product;

#Evolution; good idea gives a good idea of what's going on here.
$product{'evo'}{'pretty_name'} = 'Evolution';
$product{'evo'}{'SQL_string'} = '     
     bugs.product = \'Evolution\' or
     bugs.product = \'Bonobo\' or
     bugs.product = \'GtkHtml\' or
     bugs.product = \'GAL\' or
     bugs.product = \'GdkPixbuf\' or
     bugs.product = \'Gnome-spell\'
';
$product{'evo'}{'search_string'} = 'product=Bonobo&product=Evolution&product=GAL&product=GdkPixbuf&product=GtkHtml&product=Gnome-spell';
$product{'Evolution'}=$product{'evo'}; #ugly hack to retain 'evo' compatibility while using new menus

#Nautilus
$product{'Nautilus'}{'pretty_name'} = 'Nautilus';
$product{'Nautilus'}{'SQL_string'} = '     
     bugs.product = \'nautilus\' or
     bugs.product = \'eel\' or
     bugs.product = \'librsvg\'
';
$product{'Nautilus'}{'search_string'} = 'product=nautilus&product=eel&product=librsvg';

# If we're using bug groups for products, we should apply those restrictions
# to viewing reports, as well.  Time to check the login in that case.
ConnectToDatabase(1);
quietly_check_login();

GetVersionTable();

# Output appropriate HTTP response headers
print "Content-type: text/html\n";
# Changing attachment to inline to resolve 46897 - zach@zachlipton.com
print "Content-disposition: inline; filename=bugzilla_report.html\n\n";

PutHeader("Bug Report");

#save ourselves some writing later
my $prod_name = $::FORM{'product'};
my $keyword = $::FORM{'keyword'};
if (!defined $::FORM{'keyword'})
{
 $keyword = ''; #if no target is offered, we default to ---
}

#is the product defined in a search/sql string pair? 
#if not, we assume that the name given in $::FORM{'product'}
#is an 'exact' bugzilla name
if (defined $::FORM{'product'}) #make sure we're actually called here
{
    if(!defined $product{$prod_name}{'SQL_string'}) #we've been called but no data is available
    {
        $product{$prod_name}{'pretty_name'} = $prod_name;
        $product{$prod_name}{'SQL_string'} = '
     bugs.product = \''.$prod_name.'\'
';
        $product{$prod_name}{'search_string'} = 'product='.$prod_name;
    }
}

print <<FIN;
<center>
<h1>
$product{$prod_name}{'pretty_name'} Bug Status Report
</h1>
</center>
FIN

    if (! defined $::FORM{'product'}){
        menu();}

else {
    target_report();
}
print "<p>";

PutFooter();

sub menu {

#for the time being we generate these manually, because the alternative
# leaves us with a lot of products we can't deal with and targets we don't want
my @myproducts;
push( @myproducts, 'Nautilus');
my @mymilestones;
push( @mymilestones, 'GNOME2');

#print out a menu of choices here
#note that for the time being this menu is built rather stupidly

    my $product_popup = make_options (\@myproducts, $myproducts[0]);
    my $milestone_popup = make_options (\@mymilestones, $mymilestones[0]);

print <<FIN;
<h2>Choose a Product and Keyword for a Report</h2>
<form method=get action=product-target-report.cgi>
<table border=1 cellpadding=5>
<tr>
<td align=center><b>Product:</b></td>
<td align=center>
<select name="product">
$product_popup
</select>
</td>
</tr>
<tr>
<td align=center><b>Keyword:</b></td>
<td align=center>
<select name="keyword">
$milestone_popup
</select>
</td>
</tr>
<tr>
<td colspan=2 align=center>
<input type=submit value='Display Report'>
</td>
</tr>
</table>
</form>
<br>
If you have questions about this page, or would like to have a product or keyword added, please email bugmaster\@gnome.org.
<p>
FIN
}

sub target_report {
#report on target milestone bugs, by developer
# Build up $query string

    my $query = <<FIN;
select 
    distinct(bugs.bug_id), assign.login_name, bugs.priority, bugs.bug_severity 

from   bugs,
       profiles assign

where  bugs.assigned_to = assign.userid

and 
    (
     $product{$prod_name}{'SQL_string'}
     )
and      
    (
     bugs.bug_status = 'NEW' or 
     bugs.bug_status = 'ASSIGNED' or 
     bugs.bug_status = 'REOPENED'
     )
and
    bugs.keywords like '%$keyword%'
order by login_name
FIN
# End build up $query string
    SendSQL ($query);
    
    my $c = 0;

    my %targeted_bugs_per_devel;
    my @target_owners_list;

    my $bugs_count = 0;
    my $bugs_reopened = 0;
    my $product;
    my $priority;
    my @owners_list; #temporary hack until I figure out sorting of hashes
    my @components_list;
    my %bugs_owners;
    my %bugs_summary;
    my %bugs_status;
    my %bugs_totals;
    my %bugs_lookup;

    #############################
    # suck contents of database # 
    #############################

    while (my ($bug_id, $devel, $prio, $sever) = FetchSQLData()) {

        $targeted_bugs_per_devel{$devel}{$prio} ++;
        $targeted_bugs_per_devel{$devel}{$sever} ++;
        $targeted_bugs_per_devel{$devel}{'total'} ++;
        $targeted_bugs_per_devel{' '}{$prio} ++;
        $targeted_bugs_per_devel{' '}{$sever} ++;
        $targeted_bugs_per_devel{' '}{'total'} ++;

        #OMG this is such a nasty hack
        if($targeted_bugs_per_devel{$devel}{'first_time'}==0)
        {
            push @target_owners_list, $devel;
            $targeted_bugs_per_devel{$devel}{'first_time'} ++;
        }
    }
    push @target_owners_list, ' ';

#Severity table
#ought to be sorted by number of showstoppers
print <<FIN;
    
    <h1>$::FORM{'target'} Bug Count by Developer and Severity</h1>
    <table border=3 cellpadding=5>
    <tr>
    <td align=center bgcolor="#DDDDDD"><b>Developer</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Blocker</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Critical</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Major</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Normal</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Minor</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Trivial</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Enhancement</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Total</b></td>
    </tr>

FIN
#here we sort the developers list by 
#here we hopefully loop out the table

    foreach my $devel (@target_owners_list) {
print <<FIN;
        <tr><td align=left><tt><b>$devel</b></tt></td>
FIN
                foreach my $severity (@severities) {
                    $targeted_bugs_per_devel{$devel}{$severity}||='<font color="white">0</font>';
print <<FIN;
                    <td align=center><a href="http://bugzilla.gnome.org/buglist.cgi?$product{$prod_name}{'search_string'}&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=$severity&email1=$devel&emailtype1=exact&emailassigned_to1=1&keywords=$keyword&keywords_type=anywords">$targeted_bugs_per_devel{$devel}{$severity}</a></td>

FIN

}
print <<FIN;

        <td align=center><a href="http://bugzilla.gnome.org/buglist.cgi?$product{$prod_name}{'search_string'}&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&email1=$devel&emailtype1=exact&emailassigned_to1=1&keywords=$keyword&keywords_type=anywords">$targeted_bugs_per_devel{$devel}{'total'}</a></td></tr>

FIN

}                   
print <<FIN;
</table>
<p>

FIN

#Priority table
#ought to be sorted by number of showstoppers
print <<FIN;
    
    <h1>$::FORM{'target'} Bug Count by Developer and Priority</h1>
    <table border=3 cellpadding=5>
    <tr>
    <td align=center bgcolor="#DDDDDD"><b>Developer</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Urgent</b></td>
    <td align=center bgcolor="#DDDDDD"><b>High</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Normal</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Low</b></td>
    <td align=center bgcolor="#DDDDDD"><b>Total</b></td>
    </tr>

FIN
#here we sort the developers list by 
#here we hopefully loop out the table

    foreach my $devel (@target_owners_list) {
print <<FIN;
        <tr><td align=left><tt><b>$devel</b></tt></td>
FIN
                foreach my $priority (@priorities) {
                    $targeted_bugs_per_devel{$devel}{$priority}||='<font color="white">0</font>';
print <<FIN;
                    <td align=center><a href="http://bugzilla.gnome.org/buglist.cgi?$product{$prod_name}{'search_string'}&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&priority=$priority&email1=$devel&emailtype1=exact&emailassigned_to1=1&keywords=$keyword&keywords_type=anywords">$targeted_bugs_per_devel{$devel}{$priority}</a></td>

FIN

}
print <<FIN;

        <td align=center><a href="http://bugzilla.gnome.org/buglist.cgi?$product{$prod_name}{'search_string'}&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&email1=$devel&emailtype1=exact&emailassigned_to1=1&keywords=$keyword&keywords_type=anywords">$targeted_bugs_per_devel{$devel}{'total'}</a></td></tr>

FIN

}                   
print <<FIN;
</table>
<p>
FIN

}



