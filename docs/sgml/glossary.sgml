<!-- <!DOCTYPE glossary PUBLIC "-//OASIS//DTD DocBook V4.1//EN" > -->
<glossary id="glossary">
  <glossdiv>
    <title>0-9, high ascii</title>

    <glossentry>
      <glossterm>.htaccess</glossterm>

      <glossdef>
        <para>Apache web server, and other NCSA-compliant web servers,
        observe the convention of using files in directories called 
        <filename>.htaccess</filename>

        to restrict access to certain files. In Bugzilla, they are used
        to keep secret files which would otherwise
        compromise your installation - e.g. the 
        <filename>localconfig</filename>
        file contains the password to your database.
        curious.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-a">
    <title>A</title>

    <glossentry>
      <glossterm>Apache</glossterm>

      <glossdef>
        <para>In this context, Apache is the web server most commonly used
        for serving up 
        <glossterm>Bugzilla</glossterm>

        pages. Contrary to popular belief, the apache web server has nothing
        to do with the ancient and noble Native American tribe, but instead
        derived its name from the fact that it was 
        <quote>a patchy</quote>

        version of the original 
        <acronym>NCSA</acronym>

        world-wide-web server.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-b">
    <title>B</title>

    <glossentry>
      <glossterm>Bug</glossterm>

      <glossdef>
        <para>A 
        <quote>bug</quote>

        in Bugzilla refers to an issue entered into the database which has an
        associated number, assignments, comments, etc. Some also refer to a 
        <quote>tickets</quote>
        or 
        <quote>issues</quote>; 
        in the context of Bugzilla, they are synonymous.</para>
      </glossdef>
    </glossentry>

    <glossentry>
      <glossterm>Bug Number</glossterm>

      <glossdef>
        <para>Each Bugzilla bug is assigned a number that uniquely identifies
        that bug. The bug associated with a bug number can be pulled up via a
        query, or easily from the very front page by typing the number in the
        "Find" box.</para>
      </glossdef>
    </glossentry>

    <glossentry>
      <glossterm>Bugzilla</glossterm>

      <glossdef>
        <para>Bugzilla is the world-leading free software bug tracking system.
        </para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-c">
    <title>
    </title>

    <glossentry id="gloss-component">
      <glossterm>Component</glossterm>

      <glossdef>
        <para>A Component is a subsection of a Product. It should be a narrow
        category, tailored to your organization. All Products must contain at
        least one Component (and, as a matter of fact, creating a Product
        with no Components will create an error in Bugzilla).</para>
      </glossdef>
    </glossentry>

    <glossentry id="gloss-cpan">
      <glossterm>
        <acronym>CPAN</acronym>
      </glossterm>

      <glossdef>
        <para>
        <acronym>CPAN</acronym>

        stands for the 
        <quote>Comprehensive Perl Archive Network</quote>. 
        CPAN maintains a large number of extremely useful 
        <glossterm>Perl</glossterm>
        modules - encapsulated chunks of code for performing a
        particular task.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-d">
    <title>D</title>

    <glossentry>
      <glossterm>daemon</glossterm>

      <glossdef>
        <para>A daemon is a computer program which runs in the background. In
        general, most daemons are started at boot time via System V init
        scripts, or through RC scripts on BSD-based systems. 
        <glossterm>mysqld</glossterm>, 
        the MySQL server, and 
        <glossterm>apache</glossterm>, 
        a web server, are generally run as daemons.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-g">
    <title>
    </title>

    <glossentry>
      <glossterm>Groups</glossterm>

      <glossdef>
        <para>The word 
        <quote>Groups</quote>

        has a very special meaning to Bugzilla. Bugzilla's main security
        mechanism comes by placing users in groups, and assigning those
        groups certain privileges to view bugs in particular
        <glossterm>Products</glossterm>
        in the 
        <glossterm>Bugzilla</glossterm>
        database.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-m">
    <title>M</title>

    <glossentry>
      <glossterm>mysqld</glossterm>

      <glossdef>
        <para>mysqld is the name of the 
        <glossterm>daemon</glossterm>

        for the MySQL database. In general, it is invoked automatically
        through the use of the System V init scripts on GNU/Linux and
        AT&amp;T System V-based systems, such as Solaris and HP/UX, or
        through the RC scripts on BSD-based systems.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-p">
    <title>P</title>

    <glossentry>
      <glossterm id="gloss-product">Product</glossterm>

      <glossdef>
        <para>A Product is a broad category of types of bugs, normally
        representing a single piece of software or entity. In general,
        there are several Components to a Product. A Product may define a
        group (used for security) for all bugs entered into
        its Components.</para>
      </glossdef>
    </glossentry>

    <glossentry>
      <glossterm>Perl</glossterm>

      <glossdef>
        <para>First written by Larry Wall, Perl is a remarkable program
        language. It has the benefits of the flexibility of an interpreted
        scripting language (such as shell script), combined with the speed
        and power of a compiled language, such as C. 
        <glossterm>Bugzilla</glossterm>

        is maintained in Perl.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-q">
    <title>Q</title>

    <glossentry>
      <glossterm>QA</glossterm>

      <glossdef>
        <para>
        <quote>QA</quote>, 
        <quote>Q/A</quote>, and 
        <quote>Q.A.</quote>
        are short for 
        <quote>Quality Assurance</quote>. 
        In most large software development organizations, there is a team
        devoted to ensuring the product meets minimum standards before
        shipping. This team will also generally want to track the progress of
        bugs over their life cycle, thus the need for the 
        <quote>QA Contact</quote>

        field in a bug.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-s">
    <title>S</title>

    <glossentry>
      <glossterm>
        <acronym>SGML</acronym>
      </glossterm>

      <glossdef>
        <para>
        <acronym>SGML</acronym>

        stands for 
        <quote>Standard Generalized Markup Language</quote>. 
        Created in the 1980's to provide an extensible means to maintain
        documentation based upon content instead of presentation, 
        <acronym>SGML</acronym>

        has withstood the test of time as a robust, powerful language. 
        <glossterm>
          <acronym>XML</acronym>
        </glossterm>

        is the 
        <quote>baby brother</quote>

        of SGML; any valid 
        <acronym>XML</acronym>

        document it, by definition, a valid 
        <acronym>SGML</acronym>

        document. The document you are reading is written and maintained in 
        <acronym>SGML</acronym>, 
        and is also valid 
        <acronym>XML</acronym>

        if you modify the Document Type Definition.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-t">
    <title>T</title>

    <glossentry id="gloss-target-milestone" xreflabel="Target Milestone">
      <glossterm>Target Milestone</glossterm>

      <glossdef>
        <para>Target Milestones are Product goals. They are configurable on a
        per-Product basis. Most software development houses have a concept of
        
        <quote>milestones</quote>

        where the people funding a project expect certain functionality on
        certain dates. Bugzilla facilitates meeting these milestones by
        giving you the ability to declare by which milestone a bug will be
        fixed, or an enhancement will be implemented.</para>
      </glossdef>
    </glossentry>
  </glossdiv>

  <glossdiv id="gloss-z">
    <title>Z</title>

    <glossentry id="zarro-boogs-found" xreflabel="Zarro Boogs Found">
      <glossterm>Zarro Boogs Found</glossterm>

      <glossdef>
        <para>This is the cryptic response sent by Bugzilla when a query
        returned no results. It is just a goofy way of saying "Zero Bugs
        Found".</para>
      </glossdef>
    </glossentry>
  </glossdiv>
</glossary>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-always-quote-attributes:t
sgml-auto-insert-required-elements:t
sgml-balanced-tag-edit:t
sgml-exposed-tags:nil
sgml-general-insert-case:lower
sgml-indent-data:t
sgml-indent-step:2
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
sgml-minimize-attributes:nil
sgml-namecase-general:t
sgml-omittag:t
sgml-parent-document:("Bugzilla-Guide.sgml" "book" "chapter")
sgml-shorttag:t
sgml-tag-region-if-active:t
End:
-->

