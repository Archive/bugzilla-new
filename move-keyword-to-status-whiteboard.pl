#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-

use diagnostics;
use strict;

use lib qw(.);

require "globals.pl";
#use vars qw($template $vars);

ConnectToDatabase(1);
#GetVersionTable();

  my $keyword = $ARGV[0];
  my $namespace = $ARGV[1];
  my $loginname = $ARGV[2];

  if (!defined($keyword) || !defined($namespace) || !defined($loginname)) {
    print "Usage: keyword namespace loginname_of_user_doing_change\n";
    exit();
  }

  # 1) Verify that the keyword exists
  SendSQL("SELECT id FROM keyworddefs WHERE name=" . SqlQuote($keyword));
  my ($keywordid) = FetchSQLData() or die "No such keyword: $keyword\n";

  # 2) Find $keyword_fieldid and $status_whiteboard_fieldid
  SendSQL("SELECT fieldid FROM fielddefs WHERE name = 'keywords'");
  my ($keyword_fieldid) = FetchSQLData();
  SendSQL("SELECT fieldid FROM fielddefs WHERE name = 'status_whiteboard'");
  my ($status_whiteboard_fieldid) = FetchSQLData();

  # 3) Find $userid
  SendSQL("SELECT userid FROM profiles 
           WHERE login_name = " . SqlQuote($loginname));
  my ($userid) = FetchSQLData() or die "No such user: $loginname\n";

  # 4) Get $curtime
  SendSQL("SELECT NOW()");
  my ($curtime) = FetchSQLData();

  # 5) Find the relevant bugs
  my @sql_data = ();
  SendSQL("SELECT bugs.bug_id, bugs.keywords, bugs.status_whiteboard
           FROM bugs, keywords, keyworddefs
           WHERE keywords.bug_id=bugs.bug_id
             AND keyworddefs.id=keywords.keywordid
             AND keyworddefs.name=" . SqlQuote($keyword));
  while (my ($bug_id, $keywords, $status_whiteboard) = FetchSQLData() ) {
    PushGlobalSQLState();

    # 6) Update bugs.keywords and bugs.status_whiteboard
    $keywords =~ s/(.*), $keyword, (.*)/$1, $2/;
    $keywords =~ s/(.*), $keyword/$1/;
    $keywords =~ s/$keyword, (.*)/$1/;
    if ($keywords eq $keyword) {
      $keywords = "";
    }
    my $new_status_whiteboard = "$namespace\[$keyword\]";
    if ($status_whiteboard) {
      $status_whiteboard .= " $new_status_whiteboard";
    } else {
      $status_whiteboard = $new_status_whiteboard;
    }
    SendSQL("UPDATE bugs 
             SET delta_ts = delta_ts, 
                 keywords='$keywords', 
                 status_whiteboard=" . SqlQuote($status_whiteboard) . "
             WHERE bug_id=$bug_id");

    # 7) Record the update in bugs_activity
    SendSQL("INSERT INTO bugs_activity
             VALUES ($bug_id, $userid, '$curtime', $keyword_fieldid, 
                     " . SqlQuote($keyword). ", '', NULL)");
    SendSQL("INSERT INTO bugs_activity
             VALUES ($bug_id, $userid, '$curtime', $status_whiteboard_fieldid, 
                     '', " . SqlQuote($new_status_whiteboard). ", NULL)");

    # 8) Record the update in keywords
    SendSQL("DELETE FROM keywords 
             WHERE keywordid = $keywordid
               AND bug_id = $bug_id");

    PopGlobalSQLState();
  }

  print "All references to keyword $keyword successfully moved into the " .
        "status_whiteboard with namespace $namespace by $loginname.\n";
