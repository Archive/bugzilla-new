#!/usr/bin/perl

push @INC, "/usr/local/www/bugzilla/bugzilla";
chdir "/usr/local/www/bugzilla/bugzilla";

require "globals.pl";

ConnectToDatabase(1);
GetVersionTable();

my @products;
my $product = $ARGV[0];
my $new_version = $ARGV[1];

if (!defined($product) || !defined ($new_version))
{
	print "Usage: add-version product new-version\n";
	print "  product - the bugzilla product with a new version\n";
	print "  version - the new version that has been released\n\n";
	print "The program calculates what version to add to the database\n";
	print "(e.g. 2.0.x) based on existing versions in that product.\n\n";
	print "If that doesn't make sense, don't use this script.\n\n";
	print "Love,\n";
	print "Andrew\n";
	exit();
}

if (@products = grep (lc($_) eq lc($product), keys %::versions))
{
$product = $products[0];
}
else
{
print "Bugzilla product $product does not exist so no bugzilla updating done.\n";
exit();
}

my $version_xn = $new_version;
$version_xn =~ s/^([\d\.]+)\.\d+$/$1.x/;
my $version_x1 = $new_version;
$version_x1 =~ s/^([\d]*?)\.([\d]*?)\..*/$1\.$2\.x/;
my $version_xn_1="";

if (@version = grep /^$new_version$/i, @{$::versions{$product}})
# match on a.b.c
{
print "Bugzilla version $new_version exists for product $product\n";
exit();
}

do {
if (@version = grep /^$version_xn$/i, @{$::versions{$product}})
# match on *.x
{
print "Bugzilla version $version_xn exists for product $product\n";
exit();
}
$version_xn_1 = $version_xn;
$version_xn =~ s/\.x//;
$version_xn =~ s/^([\d\.]+)\.\d+$/$1.x/;
} until ($version_xn eq $version_xn_1);

print "Creating version $version_x1 for product $product\n";
SendSQL("INSERT INTO versions (program, value) VALUES (" . SqlQuote($product) . ", " . SqlQuote($version_x1) . ")");
unlink("data/versioncache");
exit();
