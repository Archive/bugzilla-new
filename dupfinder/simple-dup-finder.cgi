#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
#TODO:
#see also bug 87927

# Bugzilla doesn't expect us to be running in a subdirectory, so we
# do a little magic to trick it.

use Cwd;
my $dir = cwd;
if ($dir =~ /dupfinder$/) {
  chdir "..";
}
push @INC, "dupfinder/."; # many scripts now are in the reports subdirectory


use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "find-traces.pl";

#set up the page
ConnectToDatabase(1);
GetVersionTable();

# When we show the footer, it's confusing to claim the user isn't logged in
# if they are.
quietly_check_login();

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

my $bug_id = $::FORM{'id'};
my $custom_trace = $::FORM{'custom_trace'};
PutHeader("possible CRASH dups",'');

if(defined($bug_id) || defined($custom_trace)){
	print "<p>First five function calls in the bug:</p>\n" .
              "<ol>";

	my $text;
	if(defined($custom_trace)) {
		$text = $custom_trace;
	}
	else {
		$text = get_long_description_from_database($bug_id);
	}
	my @functions = get_traces_from_string($text);

	foreach my $func (@functions){
            print '<li>' . $func . '</li>';
	}

	print"</ol>";

	#There weren't any functions found, which is probably an error :) 
	if($functions[0] eq ''){
	  print "<p>
                No stack symbols were found in bug $bug_id.  This may
                represent a bug in this page; if you believe there is a
                stack trace on the page, please email the bug number $bug_id 
                to bugmaster\@gnome.org, or add a comment to
                <a href=\"../show_bug.cgi?id=87927\">bug 87927</a>.</p>";
	  exit;
	}

        my $query = url_quote( "meta-status:all " . join(' ', @functions) );

        print "\n\n<p>You can use boogle to <a href=
        \"http://bugzilla.gnome.org/reports/boogle.cgi?query=$query\">
        modify or refine this search</a>.</p>\n\n";

	print "<p>Bugs that contain all five function calls:</p>\n";

   	my $limited = show_duplicates_given_functions(@functions);

        if ($limited) {
            print "<p style=\"color: red;\">Warning: Number of bugs has been limited to 100.</p>";
        }
}
else{
	print<<FIN
	1.) Bug Number:<br>
	<form action="simple-dup-finder.cgi" method="get">
	<input type="text" name="id">
	<input type="submit" value="Go">
	</form>
	<p>
	2.) Paste Trace:<br>
	<form action="simple-dup-finder.cgi" method="post">
	<textarea name="custom_trace" cols="80" rows="25" wrap></textarea>
	<input type="submit" value="Go">
	</form>
FIN
}
print "
  <p>
  All bugs with valid strack traces should report <i>at least</i>
  themselves as duplicates of themselves (unless too many bugs were
  found, in which case only the first 100 will be shown). If they
  don't, that's a bug; please add a comment to <a
  href=\"../show_bug.cgi?id=87927\">bug 87927</a> or email
  bugmaster\@gnome.org with the bug number that does <i>not</i> report
  itself as a duplicate of itself.  </p>";

PutFooter();
