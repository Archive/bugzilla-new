#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
#TODO:
#see also bug 87927

# Bugzilla doesn't expect us to be running in a subdirectory, so we
# do a little magic to trick it.

use Cwd;
my $dir = cwd;
if ($dir =~ /dupfinder$/) {
  chdir "..";
}
push @INC, "dupfinder/."; # many scripts now are in the reports subdirectory


use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "find-traces.pl";

#set up the page
ConnectToDatabase(1);
GetVersionTable();

# Output appropriate HTTP response headers
print "Content-type: text/html\n\n";

my $bug_id = $::FORM{'id'};
my $custom_trace = $::FORM{'custom_trace'};
PutHeader("possible CRASH dup",'');

print<<FIN;
Unfortunately the simple-dup-finder has been taken down until it can be rewritten to be more performant. If you're interested in doing this work (SQL and perl experience probably pretty necessary) drop bugmaster\@gnome.org a line.
FIN
exit;

if(defined($bug_id) || defined($custom_trace)){
	print <<FIN;
	USE: simple-dup-finder.cgi?id=BUGNUMBER<br>
	First five function calls in the bug:<p>
	<ol>
FIN

	my $text;
	if(defined($custom_trace)) {
		$text = $custom_trace;
	}
	else {
		$text = get_long_description_from_database($bug_id);
	}
	my @functions = get_traces_from_string($text);

	foreach my $func (@functions){
   	 print '<li>'.$func;
	}

	print"</ol>";

	#There weren't any functions found, which is probably an error :) 
	if($functions[0] eq ''){
	print<<FIN;
	No stack symbols were found in bug $bug_id. This may represent a bug in this page; if you believe there is a stack trace on the page, please email the bug number $bug_id to bugmaster\@gnome.org, or add a comment to <a href="../show_bug.cgi?id=87927">bug 87927.</a>.
FIN
	}
}
PutFooter();
