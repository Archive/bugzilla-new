#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
#TODO:
#see also bug 87927

# Bugzilla doesn't expect us to be running in a subdirectory, so we
# do a little magic to trick it.

use Cwd;
my $dir = cwd;
if ($dir =~ /dupfinder$/) {
    chdir "..";
}
push @INC, "dupfinder/."; # many scripts now are in the reports subdirectory

use diagnostics;
use strict;

require "CGI.pl";
use vars qw(%FORM); # globals from CGI.pl

require "globals.pl";
require "find-traces.pl";

#set up the page
ConnectToDatabase(1);
GetVersionTable();

my $query='SELECT bug_id, bug_when, thetext from longdescs where thetext REGEXP "#[[:digit:]]+ +0x[0-9A-F]+ in \([[:alnum:]]+\)"';
#my $query='SELECT bug_id, bug_when, thetext from longdescs where bug_id=50100 AND thetext REGEXP "#[[:digit:]]+ +0x[0-9A-F]+ in \([[:alnum:]]+\)"';

SendSQL($query);
my @functions;
my @inserts;
my $counter=0;
my $tempval='';

my $bug_id='';
my $bug_when='';
my $text='';

while (($bug_id, $bug_when, $text) = FetchSQLData()) {
  # get functions out
    @functions = get_traces_from_string($text);
    $inserts[$counter] = 'INSERT INTO temporary_bug_assess (bug_id, bug_when, md5_of_funcs) VALUES ('.SqlQuote($bug_id).','.SqlQuote($bug_when).','.SqlQuote($functions[0].$functions[1].$functions[2].$functions[3].$functions[4]).')';
  # dump functions in, in searchable form
  #print them so I know WTF is going on
    print($bug_id.':'.$counter.':'.$functions[0].$functions[1].$functions[2].$functions[3].$functions[4].'\n');
    $counter++;
}

foreach my $query (@inserts)
{
    SendSQL($query);
}
