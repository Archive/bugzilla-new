#!/usr/bonsaitools/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-

###########################################################################
# Global definitions
###########################################################################

use diagnostics;
use strict;

use AnyDBM_File;

use lib qw(.);

require "globals.pl";
use vars qw($template $vars);

ConnectToDatabase(1);
GetVersionTable();

my $name;
foreach $name (@ARGV) {

my @row;
my @sql_data = ();

SendSQL("SELECT id FROM keyworddefs WHERE name='$name'");
@row = FetchSQLData() or die "No such keyword $name, aborting at this point";
my $k_id = $row[0];

SendSQL("SELECT bugs.bug_id, bugs.keywords FROM bugs,keywords,keyworddefs WHERE keywords.bug_id=bugs.bug_id AND keyworddefs.id=keywords.keywordid AND keyworddefs.name='$name'");

while ( @row = FetchSQLData() ) {
	$row[1] =~ s/(.*), $name, (.*)/$1, $2/;
	$row[1] =~ s/(.*), $name/$1/;
	$row[1] =~ s/$name, (.*)/$1/;
	if ($row[1] eq $name) {
		$row[1] = "";
	}

	push (@sql_data, "UPDATE bugs SET delta_ts = delta_ts, keywords='" . $row[1] .
		"' WHERE bug_id=" . $row[0]);
}

foreach my $value(@sql_data) {
	SendSQL($value);
}

SendSQL("DELETE FROM keywords WHERE keywordid=$k_id");
SendSQL("DELETE FROM keyworddefs WHERE id=$k_id");

print "Keyword $name ($k_id) deleted successfully.\n";
}
